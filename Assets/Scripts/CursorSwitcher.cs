﻿using UnityEngine;

public class CursorSwitcher : MonoBehaviour
{
    [Header("Order = {defaultCursor, friendly, enemy}")]
    [SerializeField] private CursorData[] NormalCursors = new CursorData[3];
    [SerializeField] private CursorData[] TargetingCursors = new CursorData[3];

    [SerializeField] private GameObject TileHighlighter;
    [SerializeField] private Vector3 HighlighterOffset = new Vector3(0,0.11f,0);

    private CursorData currentCursor;
    private bool cursorWasOutside;

    [System.Serializable]
    public class CursorData
    {
        public Texture2D texture;
        public Vector2 hotspot;
    }

    public enum CursorType {defaultCursor, friendly, enemy};
    public static CursorSwitcher Singleton;

    public bool UseTargetCursor;
    
    private void Awake()
    {
        Switch(CursorType.defaultCursor);
        Singleton = this;
    }

    private void OnDisable()
    {
        Switch(CursorType.defaultCursor);
        if(TileHighlighter)
            TileHighlighter.SetActive(false);
    }

    private void Switch(CursorType type, bool force = false)
    {
        int indexForm = (int)type;
        CursorData getData = UseTargetCursor ? 
            TargetingCursors[indexForm] : NormalCursors[indexForm];
        
        if(force || currentCursor != getData)
        {
            Cursor.SetCursor(getData.texture, getData.hotspot, CursorMode.Auto);
            currentCursor = getData;
        }
    }

    private void FixedUpdate()
    {
        bool forceSwitch = false;
        var position = Input.mousePosition;
        if (cursorWasOutside)
        {            
            if (0 <= position.x && position.x <= Screen.width &&
                0 <= position.y && position.y <= Screen.height)
            {
                forceSwitch = true;
                cursorWasOutside = false;
            }
        }
        else if (position.x < 0 || Screen.width < position.x ||
                position.y < 0 || Screen.height < position.y)
                cursorWasOutside = true;

        PlayerController player = PlayerController.Singleton;
        
        player.GetHitObject(out Object hitObject);
        Fighter hitFighter = hitObject as Fighter;
        EnvironmentTile hitTile = hitObject as EnvironmentTile;
        if (hitFighter)
        {
            bool isFriends = player.GetComponent<Fighter>().IsFriendly(hitFighter);
            Switch(isFriends ? CursorType.friendly : CursorType.enemy, forceSwitch);
        }
        else
        {
            Switch(CursorType.defaultCursor, forceSwitch);
        }

        if(hitTile)
        {
            TileHighlighter.SetActive(true);
            TileHighlighter.transform.position = hitTile.Position + HighlighterOffset;
        }
        else
        {
            TileHighlighter.SetActive(false);
        }
    }
}
