﻿using UnityEngine;

[CreateAssetMenu(fileName = "Merchant Collection", menuName = "MyData/Merchant Collection", order = 0)]
public class MerchantCollection : ScriptableObject
{
    private int lastIndex = -1;

    public Material[] Skins;
    public MerchantWares[] Wares;

    public Material GetRandomSkin(out MerchantWares correspondingWares)
    {
        int index;
        while ((index = Random.Range(0, Skins.Length)) == lastIndex) ;

        lastIndex = index;
        correspondingWares = Wares[index];
        return Skins[index];
    }
}