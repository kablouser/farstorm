﻿using UnityEngine;

public class SkinChanger : MonoBehaviour
{
    [SerializeField] private SkinnedMeshRenderer[] Renderers;

    public Material SelectSkin;
    
    public void GetRenderers()
    {
        Renderers = GetComponentsInChildren<SkinnedMeshRenderer>();
    }

    [ContextMenu("Change Skin",false,0)]
    public void ChangeSkin()
    {
        if(Renderers.Length == 0)
            GetRenderers();
        
        foreach(SkinnedMeshRenderer renderer in Renderers)
            renderer.material = SelectSkin;
    }
}
