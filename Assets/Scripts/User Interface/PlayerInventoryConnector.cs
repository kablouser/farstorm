﻿using UnityEngine;

public class PlayerInventoryConnector : MonoBehaviour
{
    [SerializeField] private DroppableSlot[] AllSlots = new DroppableSlot[Inventory.NumberOfSlots];
    [SerializeField] private DraggableItem DragItemPrefab;

    private Inventory playerInventory;
    private Status playerStatus;
    private RangeIndicator playerRangeIndicator;

    private int targetingItemSlot;

    public static PlayerInventoryConnector Singleton;

    private void Awake()
    {
        Singleton = this;
    }

    private void Start()
    {
        playerInventory = PlayerController.Singleton.GetComponent<Inventory>();
        playerStatus = playerInventory.GetComponent<Status>();
        playerRangeIndicator = playerInventory.GetComponentInChildren<RangeIndicator>(true);

        ItemInstance[] playerItems = playerInventory.GetItems;

        for(int i = 0; i < Inventory.NumberOfSlots; i++)
            if(playerItems[i].Item)
                AllSlots[i].UpdateItem(playerItems[i].Item, DragItemPrefab);
            else
                AllSlots[i].UpdateItem(null, DragItemPrefab);

        DroppableSlot.OnDropItem += OnDropItem;
        DraggableItem.OnDropOutside += OnDropOutside;
        DraggableItem.OnItemClick += OnItemClick;
        playerInventory.OnSlotChanged += OnSlotChanged;

        StopTargeting();
    }

    private void Update()
    {
        if (CursorSwitcher.Singleton.UseTargetCursor)
        {
            if (Input.GetButtonDown(InputStrings.Fire1))
            {
                StopTargeting();
                Ability.ActiveMode mode = playerInventory.GetItems[targetingItemSlot].Item.Active.GetActiveMode;
                if (mode == Ability.ActiveMode.Target)
                {
                    PlayerController.Singleton.GetHitObject(out Object potentialTarget);
                    Fighter target = potentialTarget as Fighter;

                    if (target != null)
                    {
                        Fighter playerFighter = PlayerController.Singleton.GetComponent<Fighter>();

                        playerFighter.AbilityTarget = target;
                        playerInventory.UseItem(targetingItemSlot);
                        playerFighter.AbilityTarget = null;
                    }
                }
                else if(mode == Ability.ActiveMode.Direction)
                {
                    playerInventory.UseItem(targetingItemSlot);
                }
            }
            else if (Input.GetButtonDown(InputStrings.Fire2))
                StopTargeting();
        }
    }

    private void OnDestroy()
    {
        // very important
        DroppableSlot.OnDropItem -= OnDropItem;
        DraggableItem.OnDropOutside -= OnDropOutside;
        DraggableItem.OnItemClick -= OnItemClick;
        playerInventory.OnSlotChanged -= OnSlotChanged;
    }

    private void OnDropItem(int intoSlot, int fromSlot)
    {
        if(playerStatus.IsAlive == true)
        {
            var itemA = playerInventory.RemoveItem(fromSlot);
            var itemB = playerInventory.AddItem(intoSlot, itemA);
            playerInventory.AddItem(fromSlot, itemB);
        }
    }

    private void OnDropOutside(int slot)
    {
        if(playerStatus.IsAlive == true)
        {
            ItemInstance item = playerInventory.RemoveItem(slot);
            if(item.Item)
            {
                playerInventory.GetComponent<Mover>().CurrentTile.DropItem(item);
            }
        }
    }

    private void OnSlotChanged(int slot)
    {
        ItemInstance[] playerItems = playerInventory.GetItems;
        if (playerItems[slot].Item)
            AllSlots[slot].UpdateItem(playerItems[slot].Item, DragItemPrefab);
        else
            AllSlots[slot].UpdateItem(null, DragItemPrefab);
    }

    private void StartTargeting(int slot, float range)
    {
        CursorSwitcher.Singleton.UseTargetCursor = true;
        targetingItemSlot = slot;
        playerRangeIndicator.SetRange(range);
    }

    private void StopTargeting()
    {
        CursorSwitcher.Singleton.UseTargetCursor = false;
        playerRangeIndicator.TurnOff();
    }

    public void OnItemClick(int slot)
    {
        if (playerStatus.IsAlive == true)
        {
            Item getItem = playerInventory.GetItems[slot].Item;

            if(getItem && getItem.Active && 
                (getItem.Active.GetActiveMode == Ability.ActiveMode.Target ||
                getItem.Active.GetActiveMode == Ability.ActiveMode.Direction))
                StartTargeting(slot, getItem.Active.GetRange);
            else
                playerInventory.UseItem(slot);
        }
    }
}
