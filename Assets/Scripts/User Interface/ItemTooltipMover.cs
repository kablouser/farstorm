﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ItemTooltipMover : MonoBehaviour
{
    private RectTransform rectTransform;
    private List<ItemHoverer> hoverTargets;

    private static ItemTooltipMover singletonRef;

    public TextMeshProUGUI Description;
    public static ItemTooltipMover Singleton
    {
        get
        {
            if (singletonRef == null)
                return singletonRef = FindObjectOfType<ItemTooltipMover>();
            else
                return singletonRef;
        }
    }

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        hoverTargets = new List<ItemHoverer>();
        singletonRef = this;
        if (Description == null)
            Description = GetComponentInChildren<TextMeshProUGUI>();
    }

    private void FixedUpdate()
    {
        ItemHoverer front = null;
        while (0 < hoverTargets.Count)
        {
            if ((front = hoverTargets[0]) == null || 
                front.isActiveAndEnabled == false)
                hoverTargets.RemoveAt(0);
            else
                break;
        }

        if (front != null)
        {
            gameObject.SetActive(true);

            Vector3[] corners = new Vector3[4];
            // i=2 is top right corner
            front.Rectangle.GetWorldCorners(corners);

            corners[2].x = Mathf.Clamp(corners[2].x, 0, Screen.width - rectTransform.rect.width);
            corners[2].y = Mathf.Clamp(corners[2].y, 0, Screen.height - rectTransform.rect.height);

            rectTransform.position = corners[2];

            Description.SetText(front.ItemDisplay.ToString());
        }
        else
            gameObject.SetActive(false);
    }

    public void AddHoverTarget(ItemHoverer target)
    {
        hoverTargets.Add(target);
        FixedUpdate();
    }

    public void RemoveHoverTarget(ItemHoverer target)
    {
        hoverTargets.RemoveAll(x => x == target);
        FixedUpdate();
    }
}
