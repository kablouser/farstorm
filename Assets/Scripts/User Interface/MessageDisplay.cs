﻿using UnityEngine;
using TMPro;
using System.Collections;

public class MessageDisplay : MonoBehaviour
{
    private CanvasGroup canvasGroup;
    private TextMeshProUGUI textDisplay;
    private RectTransform rectTransform;
    private Coroutine fadeFunction;

    public float VisibleSeconds;
    public static MessageDisplay Singleton;

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        textDisplay = GetComponent<TextMeshProUGUI>();
        rectTransform = GetComponent<RectTransform>();
        canvasGroup.alpha = 0;
        Singleton = this;
    }
    private IEnumerator FadeText()
    {
        float currentAlpha;
        while (0 < (currentAlpha = canvasGroup.alpha))
        {
            yield return new WaitForFixedUpdate();
            canvasGroup.alpha = currentAlpha - Time.fixedDeltaTime / VisibleSeconds;
        }
    }
    public void DisplayMessage(string message)
    {
        canvasGroup.alpha = 1;
        textDisplay.SetText(message);

        Vector2 position = Input.mousePosition;
        float halfWidth = rectTransform.rect.width / 2;
        position.x = Mathf.Clamp(position.x, halfWidth, Screen.width - halfWidth);
        position.y = Mathf.Clamp(position.y, 0, Screen.height - rectTransform.rect.height);
        transform.position = position;

        if(fadeFunction != null)
            StopCoroutine(fadeFunction);
        fadeFunction = StartCoroutine(FadeText());
    }
    public static void CreateMessage(string message)
    {
        Singleton.DisplayMessage(message);
    }

    public static void CreateMessage(Ability.TargetFilter failedFilter)
    {        
        if(failedFilter == Ability.TargetFilter.FriendsOnly)
            CreateMessage("Cannot target enemies");
        else if (failedFilter == Ability.TargetFilter.EnemiesOnly)
            CreateMessage("Cannot target friends");
    }
}