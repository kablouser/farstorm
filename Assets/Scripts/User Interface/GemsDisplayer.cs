﻿using UnityEngine;
using TMPro;

public class GemsDisplayer : MonoBehaviour
{
    public TextMeshProUGUI Text;

    private void FixedUpdate()
    {
        Text.text = PlayerController.Singleton.GemsPurse.ToString();
    }
}
