﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class InteractInventory : MonoBehaviour
{
    [SerializeField] private ExternalItem ExternalItemPrefab;
    [SerializeField] private Transform ScrollContent;
    
    private List<ExternalItem> displayItems;
    private Stack<ExternalItem> displayPool;

    private Func<Item[]> getItems;
    private Func<int,bool> tryTakeItem;
    private Func<int,int> getPrice;

    private const float heightPerDisplay = 100f;

    private void Awake()
    {
        displayItems = new List<ExternalItem>(5);
        displayPool = new Stack<ExternalItem>(5);
    }

    private void FixedUpdate()
    {
        if(getItems == null || tryTakeItem == null)
            return;
        
        Item[] items = getItems();
        
        if(items == null)
        {
            CloseInventory();
            return;
        }

        int i = 0;
        for(; i < items.Length; i++)
        {
            if(displayItems.Count <= i)
            {
                // displayItems not long enough
                ExternalItem newItem = GenerateExternalItem();
                newItem.ChangeItem(items[i]);
                newItem.Index = i;
                displayItems.Add(newItem);
            }
            else if(displayItems[i].GetDisplay() != items[i].Display)
            {
                displayItems[i].ChangeItem(items[i]);
            }

            if(getPrice == null)
            {
                displayItems[i].DefaultButtonText();
                displayItems[i].TakeButton.interactable = true;
            }
            else
            {
                displayItems[i].ChangeButtonText(
                    String.Format("Buy\nCost : {0} Gems",getPrice(i)));
            }
        }

        while(i < displayItems.Count)
        {
            // too many items
            ExternalItem removeItem = displayItems[i];
            displayItems.RemoveAt(i);

            removeItem.gameObject.SetActive(false);
            displayPool.Push(removeItem);
        }

        RectTransform rect = ScrollContent.GetComponent<RectTransform>();
        Vector2 size = rect.sizeDelta;
        size.y = heightPerDisplay * items.Length;
        rect.sizeDelta = size;
    }

    private ExternalItem GenerateExternalItem()
    {
        ExternalItem newItem;
        if(displayPool.Count == 0)
        {
            newItem = Instantiate(ExternalItemPrefab);
            newItem.gameObject.SetActive(true);
            newItem.InteractInventory = this;
            newItem.transform.SetParent(ScrollContent);
        }
        else
        {
            newItem = displayPool.Pop();
            newItem.gameObject.SetActive(true);
        }
        return newItem;
    }

    public void CloseInventory()
    {
        getItems = null;
        tryTakeItem = null;
        foreach(ExternalItem item in displayItems)
        {
            item.gameObject.SetActive(false);
            displayPool.Push(item);
        }   
        displayItems.Clear();
    }

    public void OpenInventory(Func<Item[]> getItems, Func<int,bool> tryTakeItem,
        Func<int,int> getPrice = null)
    {
        this.getItems = getItems;
        this.tryTakeItem = tryTakeItem;
        this.getPrice = getPrice;

        if(getItems == null || tryTakeItem == null)
            CloseInventory();
    }

    public void TakeItem(int index)
    {
        tryTakeItem(index);
    }
}
