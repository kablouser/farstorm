﻿using UnityEngine;
using TMPro;

public class LevelDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI TextDisplay;
    [SerializeField] private LevelManager LevelManager;

    private void Awake()
    {
        if(LevelManager == null)
            LevelManager = FindObjectOfType<LevelManager>();
    }
    
    private void FixedUpdate()
    {
        TextDisplay.SetText("<size=24>level</size> "+LevelManager.GetLevel);
    }
}
