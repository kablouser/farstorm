﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DroppableSlot : MonoBehaviour, IDropHandler
{
    [SerializeField]
    private AudioClip ClickSound;
    [Range(0, Inventory.NumberOfSlots - 1)]
    public int SlotNumber;

    // (int intoSlot, int fromSlot)
    public static event System.Action<int, int> OnDropItem;

    public DraggableItem GetDragItem
    {
        get
        {
            if(0 < transform.childCount)
                return transform.GetChild(0).GetComponent<DraggableItem>();
            else
                return null;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        // only process drops when play is alive
        if (PlayerController.Singleton.GetComponent<Status>().IsAlive == true &&
            DraggableItem.DraggedItem)
        {
            DraggableItem.DraggedItem.transform.SetParent(transform);
            DraggableItem.DraggedItem.transform.position = transform.position;
            Camera.main.GetComponent<AudioSource>().PlayOneShot(ClickSound);
        }

        if (OnDropItem != null &&
            SlotNumber != DraggableItem.DraggedItem.StartSlot.SlotNumber)
            OnDropItem(SlotNumber, DraggableItem.DraggedItem.StartSlot.SlotNumber);
    }

    public void UpdateItem(Item item, DraggableItem dragItemPrefab)
    {
        DraggableItem currentItem = GetDragItem;
        
        if(item == null)
        {
            if (currentItem)
            {
                currentItem.transform.parent = null;
                Destroy(currentItem.gameObject);                
            }
        }
        else
        {
            if(!currentItem)
                currentItem = Instantiate(dragItemPrefab,transform.position, Quaternion.identity, transform);
            
            currentItem.ChangeItem(item);
        }
    }
}