﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WorldInteraction : MonoBehaviour
{
    // larger index = rendered on top
    [SerializeField] private Button PickupItemsButton;
    [SerializeField] private Button[] MerchantButtons = new Button[4];
    [SerializeField] private InteractInventory InteractInventory;

    [SerializeField] private AudioClip NormalClickSound;
    [SerializeField] private AudioClip[] OpenPouchSounds;

    private PlayerController player;
    private Mover playerMover;
    private Inventory playerInventory;
    private Status playerStatus;
    private float[] distancesToCamera = new float[5];

    private readonly Vector3 pickupButtonOffset = -Vector3.up * .5f;
    private readonly Vector3 merchantButtonOffset = Vector3.up * 1.5f;
    
    private const string takeText = "Take";
    private const string openText = "Open";

    private void Start()
    {
        player = PlayerController.Singleton;
        playerMover = player.GetComponent<Mover>();
        playerInventory = player.GetComponent<Inventory>();
        playerStatus = player.GetComponent<Status>();

        if(InteractInventory == null)
            InteractInventory = FindObjectOfType<InteractInventory>();

        playerMover.OnRouteChanged += OnRouteChanged;
    }

    private void OnDestroy()
    {
        playerMover.OnRouteChanged -= OnRouteChanged;
    }

    private void OnRouteChanged(EnvironmentTile[] tiles)
    {
        InteractInventory.CloseInventory();
    }

    private void FixedUpdate()
    {
        EnvironmentTile playerTile;
        if(playerStatus.IsAlive == false)
            playerTile = null;
        else
            playerTile = playerMover.CurrentTile;
        
        if(playerTile == null)
        {
            PickupItemsButton.gameObject.SetActive(false);

            foreach(var button in MerchantButtons)
                button.gameObject.SetActive(false);
        }
        else
        {
            PickUpGems(playerTile);

            int itemLength = playerTile.DroppedItems.Count;

            PickupItemsButton.gameObject.SetActive(0 < itemLength);
            PickupItemsButton.transform.position = 
            Camera.main.WorldToScreenPoint(playerTile.Position + pickupButtonOffset);

            TextMeshProUGUI buttonText = 
            PickupItemsButton.GetComponentInChildren<TextMeshProUGUI>();
            
            if(1 < itemLength)
                buttonText.SetText(openText);
            else
                buttonText.SetText(takeText);

            distancesToCamera[4] =
                (playerTile.Position - Camera.main.transform.position)
                .sqrMagnitude;
            
            for(int i = 0; i < playerTile.Connections.Count; i++)
            {
                Merchant getMerchant = null;

                if (playerTile.Connections[i].Occupier)
                {
                    getMerchant = playerTile.Connections[i].Occupier.GetComponent<Merchant>();

                    if (getMerchant && getMerchant.GetItems() == null)
                        getMerchant = null;
                }
                
                MerchantButtons[i].gameObject.SetActive(getMerchant != null);

                if(getMerchant != null)
                {
                    MerchantButtons[i].GetComponent<MerchantPosition>().StoredMerchant = getMerchant;

                    MerchantButtons[i].transform.position =
                    Camera.main.WorldToScreenPoint(
                        getMerchant.transform.position + merchantButtonOffset);
                    
                    distancesToCamera[i] =
                    (getMerchant.transform.position - Camera.main.transform.position)
                    .sqrMagnitude;
                }
                else
                {
                    distancesToCamera[i] = -1;
                }
            }
            for(int i = playerTile.Connections.Count; i < 4; i++)
                MerchantButtons[i].gameObject.SetActive(false);

            List<int> sortButtons = new List<int>() {0, 1, 2, 3, 4};
            // sorts in descending order of distance-to-camera
            sortButtons.Sort(delegate(int x, int y)
            {
                if (distancesToCamera[x] == -1 && distancesToCamera[y] == -1) return 0;
                else if (distancesToCamera[x] == -1) return 1;
                else if (distancesToCamera[y] == -1) return -1;
                else return 
                0 < distancesToCamera[y] - distancesToCamera[x] ? 1 : -1;
            });
            
            for(int i = 0; i < 5; i++)
            {
                if(sortButtons[i] == 4)
                    PickupItemsButton.transform.SetSiblingIndex(i);
                else
                    MerchantButtons[sortButtons[i]].transform.SetSiblingIndex(i);                
            }
        }
    }

    private void PickUpGems(EnvironmentTile fromTile)
    {
        for(int i = 0; i < fromTile.DroppedItems.Count; i++)
        {
            if (GemItem.IsGemItem(fromTile.DroppedItems[i].Item))
            {
                if(playerInventory.AddItem(new ItemInstance(fromTile.DroppedItems[i].Item),false)
                    == false)
                {
                    var temp = playerInventory.RemoveItem(0);
                    playerInventory.AddItem(new ItemInstance(fromTile.DroppedItems[i].Item));
                    playerInventory.AddItem(temp);
                }
                
                fromTile.PickupItem(i);
            }
        }
    }

    public void PickUp()
    {
        EnvironmentTile playerTile = playerMover.CurrentTile;

        if(playerTile != null)
        {
            List<ItemInstance> tileItems = playerTile.DroppedItems;
            if(tileItems.Count == 0)
            {
                return;
            }
            else if(tileItems.Count == 1)
            {
                if(playerMover.GetComponent<Inventory>().AddItem(tileItems[0]))
                    playerTile.PickupItem(tileItems[0]);
            }
            else
            {
                InteractInventory.OpenInventory(
                    () => {
                        Item[] pullItems = new Item[
                            playerMover.CurrentTile.DroppedItems.Count];
                        for(int i = 0; i < playerMover.CurrentTile.DroppedItems.Count; i++)
                            pullItems[i] = playerMover.CurrentTile.DroppedItems[i].Item;
                        return pullItems;
                        },

                    (index) => 
                    {
                        ItemInstance pickItem = playerTile.DroppedItems[index];
                        bool success = playerInventory.AddItem(pickItem);

                        if (success)
                            playerMover.CurrentTile.PickupItem(index);
                        else
                            MessageDisplay.CreateMessage("Inventory is full");
                        return success;
                    });
            }
        }
    }

    public void OnClickMerchant(int index)
    {
        Merchant getMerchant = MerchantButtons[index].GetComponent<MerchantPosition>().StoredMerchant;
        if(getMerchant != null)
        {
            InteractInventory.OpenInventory(
                () =>
                {
                    var getList = getMerchant.GetItems();
                    if (getList != null)
                        return getList.ToArray();
                    else
                        return null;
                },

                (buyIndex) =>
                {
                    if (getMerchant == null)
                        return false;

                    var getList = getMerchant.GetItems();
                    if (getList == null)
                        return false;

                    Item pickItem = getList[buyIndex];
                    int cost = getMerchant.GetPrices()[buyIndex];

                    bool success = getMerchant.CheckPurchase(player.GemsPurse, buyIndex) &&
                                   playerInventory.AddItem(new ItemInstance(pickItem)) &&
                                   getMerchant.BuyItem(player.GemsPurse, buyIndex);
                    
                    if(success)
                        player.GemsPurse -= cost;

                    return success;
                },

                (priceIndex) =>
                {
                    if (getMerchant == null)
                        return 0;
                    return getMerchant.GetPrices()[priceIndex];
                }
                );
        }
    }

    public void PickButtonSound()
    {
        var text = PickupItemsButton.GetComponentInChildren<TextMeshProUGUI>();
        if (text.text == takeText)
            Camera.main.GetComponent<AudioSource>().PlayOneShot(NormalClickSound);
        else if(text.text == openText)
            Camera.main.GetComponent<AudioSource>().PlayOneShot(OpenPouchSounds[Random.Range(0,OpenPouchSounds.Length)]);
    }
}