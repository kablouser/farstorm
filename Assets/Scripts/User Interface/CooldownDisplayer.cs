﻿using UnityEngine;
using UnityEngine.UI;

public class CooldownDisplayer : MonoBehaviour
{
    [SerializeField] private Image[] Displayers = new Image[Inventory.NumberOfSlots];

    private Inventory playerInventory;

    private void Start()
    {
        playerInventory = PlayerController.Singleton.GetComponent<Inventory>();
    }

    private void FixedUpdate()
    {
        if(playerInventory.isActiveAndEnabled)
        {
            for(int i = 0; i < Inventory.NumberOfSlots; i++)
            {
                float remainingTime = playerInventory.GetCooldown(i) - Time.time;
                if(playerInventory.GetItems[i].Item && 0 < remainingTime)
                {
                    Displayers[i].fillAmount = 
                    remainingTime / playerInventory.GetItems[i].Item.ActiveCooldown;
                }
                else
                {
                    Displayers[i].fillAmount = 0;
                }
            }
        }
        else
        {
            for(int i = 0; i < Inventory.NumberOfSlots; i++)
            {
                Displayers[i].fillAmount = 0;
            }
        }
    }
}
