﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class PauseMenu : MonoBehaviour
{
    [SerializeField]
    private AudioMixer MasterMixer;
    [SerializeField]
    private GameObject PausePanel;

    [SerializeField]
    private Slider SoundFxSlider;
    [SerializeField]
    private Slider MusicSlider;

    private void Start()
    {
        float volume;
        if(MasterMixer.GetFloat("SoundFX", out volume))
            SoundFxSlider.value = volume;

        if (MasterMixer.GetFloat("BGM", out volume))
            MusicSlider.value = volume;
    }
    private void Update()
    {
        if (Input.GetButtonDown(InputStrings.Cancel))
        {
            TogglePause(Time.timeScale != 0);
        }
    }

    public void SetSoundFXVolume(float percent)
    {
        MasterMixer.SetFloat("SoundFX", percent);
    }

    public void SetMusicVolume(float percent)
    {
        MasterMixer.SetFloat("BGM", percent);
    }

    public void TogglePause(bool isPaused)
    {
        Time.timeScale = isPaused ? 0 : 1;
        PausePanel.SetActive(isPaused);
    }
}