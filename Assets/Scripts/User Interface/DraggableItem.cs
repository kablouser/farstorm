﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DraggableItem : MonoBehaviour, IBeginDragHandler, IDragHandler, 
    IEndDragHandler, IDropHandler, IPointerClickHandler
{
    [SerializeField]
    private AudioClip ClickSound;

    private Vector3 startPosition;
    // the parent of this object during dragging
    private Transform dragParent;
    private Image image;
    private ItemHoverer itemHoverer;

    private static float mouseDownTime;
    private const float maxClickTime = 0.5f;

    public DroppableSlot StartSlot {get; private set;}
    
    public static DraggableItem DraggedItem; //itemBeingDragged
    // when item is dropped outside of inventory
    public static event System.Action<int> OnDropOutside;
    // when any item is clicked
    public static event System.Action<int> OnItemClick;

    private void Awake()
    {
        dragParent = GetComponentInParent<Canvas>().transform;
        image = GetComponent<Image>();
        itemHoverer = GetComponent<ItemHoverer>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button != PointerEventData.InputButton.Left)
            return;

        OnItemClick?.Invoke(transform.parent.GetComponent<DroppableSlot>().SlotNumber);
        Camera.main.GetComponent<AudioSource>().PlayOneShot(ClickSound);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        DraggedItem = this;
        startPosition = transform.position;
        StartSlot = transform.parent.GetComponent<DroppableSlot>();
        GetComponent<CanvasGroup>().blocksRaycasts = false;

        transform.SetParent(dragParent);

        Camera.main.GetComponent<AudioSource>().PlayOneShot(ClickSound);
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        DraggedItem = null;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        
        if (transform.parent == StartSlot.transform || transform.parent == dragParent)
        {
            bool droppedOutside = transform.parent == dragParent;
            
            transform.position = startPosition;
            transform.SetParent(StartSlot.transform);

            if(droppedOutside)
                OnDropOutside(StartSlot.SlotNumber);
        }        
    }

    public void OnDrop(PointerEventData eventData)
    {
        transform.parent.GetComponent<DroppableSlot>().OnDrop(eventData);
    }

    public void ChangeItem(Item item)
    {
        image.sprite = item.Display;
        itemHoverer.ChangeItem(item);
    }
}