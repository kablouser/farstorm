﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ExternalItem : MonoBehaviour
{
    public Image DisplayImage;
    public int Index;
    public InteractInventory InteractInventory;
    public Button TakeButton;
    public TextMeshProUGUI TakeButtonText;

    private string defaultText;
    private ItemHoverer itemHoverer;

    private void Awake()
    {
        defaultText = TakeButtonText.text;
        itemHoverer = GetComponent<ItemHoverer>();
    }

    public void ChangeItem(Item item)
    {
        DisplayImage.sprite = item.Display;
        itemHoverer.ChangeItem(item);
    }

    public Sprite GetDisplay()
    {
        return DisplayImage.sprite;
    }

    public void TakeItem()
    {
        InteractInventory.TakeItem(Index);
    }

    public void ChangeButtonText(string newText)
    {
        TakeButtonText.text = newText;
    }

    public void DefaultButtonText()
    {
        TakeButtonText.text = defaultText;
    }
}
