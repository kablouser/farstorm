﻿using UnityEngine;
using System.Text;
using TMPro;

public class PlayerStatusDisplayer : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI ValuesDisplay;
    private Status playerStatus;
    private void Start()
    {
        playerStatus = PlayerController.Singleton.GetComponent<Status>();
    }

    private void FixedUpdate()
    {
        Resource currentResource = playerStatus.GetResource;
        Resource baseResource = playerStatus.BaseResource;
        Resource difference = currentResource - baseResource;

        float[] baseValues = baseResource.ToArray();
        float[] differenceValues = difference.ToArray();        

        StringBuilder stringBuilder = new StringBuilder(
            string.Format("{0}/{1}\n", RoundNumber(currentResource.health), RoundNumber(baseResource.health)));

        for(int i = 1; i < baseValues.Length; i++)
        {
            if (differenceValues[i] == 0)
            {
                stringBuilder.Append(
                    string.Format("{0}\n", RoundNumber(baseValues[i])));
            }
            else
            {                
                char sign = 0 < differenceValues[i] ? '+' : '-';
                stringBuilder.Append(
                    string.Format("{0}{1}{2}\n", RoundNumber(Mathf.Abs(baseValues[i])), sign, RoundNumber(Mathf.Abs(differenceValues[i]))));
            }
        }

        ValuesDisplay.SetText(stringBuilder.ToString());
    }

    private float RoundNumber(float value, bool ceil = false)
    {
        if(ceil)
            return Mathf.Ceil(value * 100) / 100;
        else
            return Mathf.Round(value * 100) / 100;
    }
}
