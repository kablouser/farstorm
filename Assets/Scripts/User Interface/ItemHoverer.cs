﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ItemHoverer : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Item ItemDisplay { get; private set; }
    public RectTransform Rectangle;

    private void Awake()
    {
        Rectangle = GetComponent<RectTransform>();
    }

    private void OnDisable()
    {
        ItemTooltipMover.Singleton.RemoveHoverTarget(this);
    }

    private void OnDestroy()
    {
        ItemTooltipMover.Singleton.RemoveHoverTarget(this);
    }

    public void ChangeItem(Item item)
    {
        ItemDisplay = item;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ItemTooltipMover.Singleton.AddHoverTarget(this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ItemTooltipMover.Singleton.RemoveHoverTarget(this);
    }
}