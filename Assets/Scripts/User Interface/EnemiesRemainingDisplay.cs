﻿using UnityEngine;
using TMPro;

public class EnemiesRemainingDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI TextDisplay;

    private void FixedUpdate()
    {
        TextDisplay.SetText(
            Environment.Singleton.GetRemainingEnemies + " <size=24>enemies left");
    }            
}
