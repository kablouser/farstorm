﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    private Environment environment;
    private CharacterAnimation animator;
    private Status status;

    private bool isMoving;
    private bool previousIsMoving;

    private Vector3 destination;
    private Vector3 moveVector;

    private Queue<EnvironmentTile> route;
    private EnvironmentTile lastFollowTile;

    public EnvironmentTile CurrentTile;
    public Mover FollowMover { get; private set; }

    public bool IsLocked;

    public event Action<EnvironmentTile[]> OnRouteChanged;

    private void Awake()
    {
        environment = Environment.Singleton;
        animator = GetComponent<CharacterAnimation>();
        route = new Queue<EnvironmentTile>();
        status = GetComponent<Status>();
    }

    private void OnDisable()
    {
        isMoving = false;
        previousIsMoving = true;
        Stop();

        if(CurrentTile != null)
        {
            CurrentTile.Occupier = null;
        }
    }

    private void FixedUpdate()
    {        
        if(isMoving)
        {
            Vector3 currentVector = destination - transform.position;
            if(0 < Vector3.Dot(currentVector, moveVector))
            {
                // vectors pointing the same direction
                transform.position +=
                moveVector * status.GetResource.movementSpeed * Environment.TileSize * Time.fixedDeltaTime;
                
                animator.SetSpeed(CharacterAnimation.SpeedParam.walkSpeed, status.GetResource.movementSpeed);
            }
            else
            {
                transform.position = destination;
                isMoving = false;
            }
        }

        if(FollowMover && lastFollowTile != FollowMover.CurrentTile)
        {
            UpdateFollow();
        }

        if(!isMoving && route != null && 0 < route.Count && IsLocked == false)
        {
            EnvironmentTile nextTile = null;
            while((nextTile = route.Dequeue()) == CurrentTile)
            {
                if(0 == route.Count)
                {
                    nextTile = null;
                    break;
                }
            }

            if(nextTile == null) {} // no need to follow route. (already arrived?)
            else if(!CurrentTile.IsAdjacent(nextTile))
            {
                route.Clear();
                Debug.LogError("Route is not connected! "+CurrentTile.name+", "+nextTile.name);
            }
            else if(nextTile.TryOccupy(this))
            {
                SetDestination(nextTile.Position);
            }
            else
            {
                Debug.LogWarningFormat(
                    "Path blocked by something! IsAccessible = {0}, Occupier = {1}",
                    nextTile.IsAccessible,
                    nextTile.Occupier);
                
                // do some wizardary here
                route.Clear();
            }

            RouteChanged(route.ToArray());
        }

        // isMoving has changed!
        if(previousIsMoving != isMoving)
        {
            if(isMoving)
            {
                animator.Play(CharacterAnimation.Parameter.walk);
            }
            else
            {
                animator.Play(CharacterAnimation.Parameter.idle);
            }
        }
        previousIsMoving = isMoving;
    }

    private void UpdateFollow()
    {
        // if no follow target, do nothing
        if(!FollowMover)
            return;
        lastFollowTile = FollowMover.CurrentTile;
        GoTo(lastFollowTile, false);
    }

    private void SetDestination(Vector3 newDestination)
    {
        isMoving = true;
        destination = newDestination;

        moveVector = destination - transform.position;
        if(moveVector.magnitude != 0)
        {
            transform.rotation = Quaternion.LookRotation(moveVector, Vector3.up);
            moveVector.Normalize();
        }
    }

    private void RouteChanged(EnvironmentTile[] route)
    {
        OnRouteChanged?.Invoke(route);
    }

    private void SetRoute(List<EnvironmentTile> route)
    {
        if(route != null)
        {
            this.route = new Queue<EnvironmentTile>(route);
            EnvironmentTile nextTile = this.route.Peek();
            
            if(nextTile == CurrentTile)
            {
                this.route.Dequeue();
                if(0 < this.route.Count)
                    nextTile = this.route.Peek();
                else
                    nextTile = null;
            }

            if(nextTile != null)
            {
                Vector3 moveVectorNext = nextTile.Position - CurrentTile.Position;

                if(isMoving && Vector3.Dot(moveVector, moveVectorNext) < -0.9f)
                {
                    // they are pointing in opposite directions,
                    // you need to go backwards!

                    if(nextTile.TryOccupy(this))
                    {
                        SetDestination(nextTile.Position);
                    }
                }
            }

            RouteChanged(this.route.ToArray());
        }
    }

    private IEnumerator MoveInRangeRoutine(Mover targetMover, int slot, float range)
    {
        bool abort = false;
        Inventory thisInventory = GetComponent<Inventory>();

        Follow(targetMover);
        System.Action<int> inventoryChanged = i => { if (i == slot) abort = true; };
        thisInventory.OnSlotChanged += inventoryChanged;

        do
        {
            yield return new WaitForFixedUpdate();
            if (FollowMover != targetMover)
            {
                abort = true;
                break;
            }
        }
        while (abort == false &&
            range < Vector3.Distance(transform.position, targetMover.transform.position));

        if (abort == false)
        {
            var fighter = GetComponent<Fighter>();
            fighter.AbilityTarget = targetMover.GetComponent<Fighter>();

            thisInventory.UseItem(slot);
            fighter.AbilityTarget = null;
            Stop();
        }

        thisInventory.OnSlotChanged -= inventoryChanged;
    }

    public void GoTo(EnvironmentTile setDestination, bool stopFollowing = true)
    {
        if(stopFollowing)
            FollowMover = null;
        
        List<EnvironmentTile> newRoute = environment.Solve(CurrentTile, setDestination, out bool success);
        // only change our route if the solve was successful
        if(success)
            SetRoute(newRoute);
    }

    public void Follow(Mover follow)
    {
        if(follow == FollowMover)
            return;

        FollowMover = follow;
        UpdateFollow();
    }

    public void Stop(bool stopFollowing = true)
    {
        if(stopFollowing)
            FollowMover = null;

        route.Clear();        
        RouteChanged(route.ToArray());
    }

    public void MoveInRange(Mover targetMover, int slot, float range)
    {
        StartCoroutine(MoveInRangeRoutine(targetMover, slot, range));
    }
}