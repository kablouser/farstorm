﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LootTable", menuName = "MyData/LootTable", order = 0)]
public class LootTable : ScriptableObject
{
    public Item[] ItemDrops;
    public float[] DropRates;

    public List<Item> GenerateDrops()
    {
        List<Item> drops = new List<Item>();

        for(int i = 0; i < ItemDrops.Length; i++)
        {
            if(Random.value < DropRates[i])
                drops.Add(ItemDrops[i]);
        }
        
        return drops;
    }
}