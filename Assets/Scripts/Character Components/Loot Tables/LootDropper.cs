﻿using UnityEngine;

public class LootDropper : MonoBehaviour
{
    private Mover mover;

    public LootTable LootTable;

    private void Awake()
    {
        mover = GetComponent<Mover>();
    }

    private void OnDisable()
    {
        var drops = LootTable.GenerateDrops();
        foreach(var drop in drops)
        {
            mover.CurrentTile.DropItem(new ItemInstance(drop));
        }
    }
}
