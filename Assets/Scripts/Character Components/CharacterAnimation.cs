﻿using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimation : MonoBehaviour
{
    private Animator characterAnimator;

    [SerializeField] private List<string> AnimationParameters = new List<string>() 
        {"idle", "attack", "dead", "walk"};
    [SerializeField] private List<string> AnimationSpeedParams = new List<string>()
        {"attack speed", "walk speed"};
    [SerializeField] private bool specialIdle = false;

    public enum Parameter {idle, attack, dead, walk};
    public enum SpeedParam {attackSpeed, walkSpeed};

    private void Awake() 
    {
        characterAnimator = GetComponentInChildren<Animator>();
        characterAnimator.SetBool("special idle", specialIdle);
    }

    public void Play(Parameter parameter)
    {
        characterAnimator.SetTrigger(AnimationParameters[(int)parameter]);

        //force the trigger to be processed
        characterAnimator.Update(0);
    }
    
    public void SetSpeed(SpeedParam speedParam, float multiplier)
    {
        characterAnimator.SetFloat(AnimationSpeedParams[(int)speedParam], multiplier);
    }
}