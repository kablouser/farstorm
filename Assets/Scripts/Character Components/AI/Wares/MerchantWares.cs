using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MerchantWares", menuName = "MyData/MerchantWares", order = 0)]
public class MerchantWares : ScriptableObject
{
    [System.Serializable]
    private struct TradeOption
    {
        public Item SaleItem;
        public int Price;
    }
    [SerializeField]
    private List<TradeOption> TradeOptions;
    [SerializeField]
    private int AverageNumberOfDeals = 5;
    [SerializeField]
    private int AverageVariation = 2;

    public void GenerateNewWares(out List<Item> saleItems, out List<int> salePrices)
    {
        int deals = Random.Range(AverageNumberOfDeals - AverageVariation, AverageNumberOfDeals + AverageVariation + 1);
        if(deals <= 0)
            deals = 1;
        
        List<bool> hasDoneDeal = new List<bool> (new bool[TradeOptions.Count]);

        saleItems = new List<Item>(deals);
        salePrices = new List<int>(deals);

        int counter = 0;
        while(0 < deals)
        {
            int randomIndex;
            if(TradeOptions.Count < ++counter)
                randomIndex = Random.Range(0, hasDoneDeal.Count);
            else
                while( hasDoneDeal[randomIndex = Random.Range(0, hasDoneDeal.Count)] == true);
            
            hasDoneDeal[randomIndex] = true;
            deals--;

            saleItems.Add(TradeOptions[randomIndex].SaleItem);
            salePrices.Add(Mathf.CeilToInt(Random.Range(.5f, 1.5f) * TradeOptions[randomIndex].Price));
        }
    }
}