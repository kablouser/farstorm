﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    private enum State {idle, patrol, attack};

    [SerializeField] private TriggerSensor Sensor;
    [SerializeField] private float changeStateCooldown = 2.0f;
    [SerializeField] private float boredomTime = 6.0f;

    private Mover mover;
    private Fighter fighter;
    private Status status;
    private Inventory inventory;

    private Fighter target;

    private State state;
    private float[] stateLastChanges;

    private void Awake()
    {
        if(Sensor == null)
            Sensor = GetComponentInChildren<TriggerSensor>();
        
        mover = GetComponent<Mover>();
        fighter = GetComponent<Fighter>();
        status = GetComponent<Status>();
        inventory = GetComponent<Inventory>();

        stateLastChanges = new float[3];
    }

    private void OnEnable()
    {
        Sensor.Sensed = Sensed;
        status.OnHit += OnHit;

        target = null;
    }

    private void OnDisable() => status.OnHit -= OnHit;

    private void FixedUpdate()
    {
        if(state == State.idle)
        {
            if(ChangeState(State.patrol))
            {                
                EnvironmentTile tile = null;
                while(tile == null)
                {
                    float randomAngle = Random.Range(0,2 * Mathf.PI);
                    Vector3 moveDirection = new Vector3(
                        Mathf.Cos(randomAngle),0,
                        Mathf.Sin(randomAngle));
                    moveDirection *= status.GetResource.movementSpeed * boredomTime
                        * Random.Range(0.6f, 1);
                    tile = Environment.Singleton.TileAt(
                        transform.position + moveDirection);
                }
                mover.GoTo(tile);
            }
        }
        else if(state == State.patrol) {}
        else if(state == State.attack)
        {
            if(target == null || target.isActiveAndEnabled == false)
                ChangeState(State.idle);
            else
            {
                fighter.TryAttack(target);
                if(fighter.InRange(target))
                    stateLastChanges[(int)State.attack] = Time.time; // reset boredom

                fighter.AbilityTarget = target;
                for (int i = 0; i < Inventory.NumberOfSlots; i++)
                    inventory.UseItem(i);
            }
        }
        
        if(stateLastChanges[(int)state] + boredomTime < Time.time)
        {
            ChangeState(State.idle);
        }
    }

    private void Sensed(Collider other)
    {
        Fighter otherFighter = other.GetComponent<Fighter>();
        if(otherFighter != null && fighter.IsFriendly(otherFighter) == false)
        {
            if(target == null && ChangeState(State.attack, true))
                target = otherFighter;
        }
    }

    private void OnHit(Fighter fromFighter)
    {
        if(ChangeState(State.attack))
        {
            target = fromFighter;
        }
    }

    private void RemoveTarget()
    {
        target = null;
        fighter.StopAttack();
    }

    // urgency [0,1]
    private bool ChangeState(State newState, bool forced = false)
    {
        if(forced || stateLastChanges[(int)newState] + changeStateCooldown < Time.time)
        {
            if(state == State.attack)
                RemoveTarget();
            stateLastChanges[(int)state] =  Time.time;

            state = newState;
            stateLastChanges[(int)newState] = Time.time;
            if(newState == State.idle)
                mover.Stop();

            return true;
        }
        else return false;
    }
}
