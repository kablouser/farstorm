﻿using System.Collections.Generic;
using UnityEngine;

public class Merchant : MonoBehaviour
{
    private const float runAwayChangeCooldown = 1f;

    [SerializeField] private List<Item> ItemsForSale;
    [SerializeField] private List<int> Prices;
    [SerializeField] private MerchantCollection MerchantFamily;
    [SerializeField] private float runAwayDistance = 10f;

    private Status status;
    private Mover mover;

    private Fighter runAwayFrom;
    private float runAwayChange;

    private const float maxMerchantDistance = 1.6f;

    private void Awake()
    {
        status = GetComponent<Status>();
        mover = GetComponent<Mover>();

        SkinChanger skinChanger = GetComponentInChildren<SkinChanger>();
        skinChanger.SelectSkin = MerchantFamily.GetRandomSkin(out MerchantWares wares);
        skinChanger.ChangeSkin();
        wares.GenerateNewWares(out ItemsForSale, out Prices);
    }

    private void Start()
    {
        List<EnvironmentTile> connections = new List<EnvironmentTile>();
        connections.AddRange(mover.CurrentTile.Connections);
        int randomIndex = 2;
        while(0 < connections.Count &&
            connections[randomIndex].IsAccessible == false)
        {
            randomIndex = Random.Range(0,connections.Count);
        }
        transform.LookAt(connections[randomIndex].Position, Vector3.up);
    }

    private void OnEnable() => GetComponent<Status>().OnHit += RunAway;
    private void OnDisable() => GetComponent<Status>().OnHit -= RunAway;

    private void RunAway(Fighter fromFighter)
    {
        runAwayFrom = fromFighter;     
    }

    private void FixedUpdate()
    {
        if(runAwayFrom != null)
        {
            if(runAwayFrom.isActiveAndEnabled == false ||
                runAwayDistance < Vector3.Distance(
                    runAwayFrom.transform.position, transform.position))
                runAwayFrom = null;
            else if(runAwayChange < Time.time)
            {
                runAwayChange = Time.time + runAwayChangeCooldown;

                Vector3 awayDirection = transform.position - runAwayFrom.transform.position;
                awayDirection.Normalize();
                EnvironmentTile awayTile = null;
                bool accessible = false;
                float distance = 1;
                while(accessible == false)
                {
                    awayTile = Environment.Singleton.TileAt(
                    mover.CurrentTile.Position + awayDirection * distance);

                    if(awayTile == null)
                        break;

                    accessible = awayTile.IsAccessible;
                    distance++;
                }
                if(awayTile != null)
                    mover.GoTo(awayTile);
            }
        }
    }

    public void Restock(Item[] newItems, int[] newPrices)
    {
        if(newItems.Length != newPrices.Length)
            throw new System.ArgumentException("Both arrays must have equal length");

        ItemsForSale.Clear();
        Prices.Clear();

        ItemsForSale.AddRange(newItems);
        Prices.AddRange(newPrices);
    }

    public List<Item> GetItems()
    {
        if (runAwayFrom != null || ItemsForSale.Count == 0)
            return null;

        // looks at player
        foreach(var tile in mover.CurrentTile.Connections)
            if(tile.Occupier != null &&
               tile.Occupier.GetComponent<PlayerController>() != null)
            {
                Vector3 buyerPosition = tile.Occupier.transform.position;
                if (maxMerchantDistance < Vector3.Distance(transform.position, buyerPosition))
                {
                    return null;
                }
                else
                {
                    transform.LookAt(buyerPosition);
                    mover.Stop();
                    return ItemsForSale;
                }
            }

        return null;
    }

    public int[] GetPrices()
    {
        return Prices.ToArray();
    }

    public bool CheckPurchase(int purse, int index)
    {
        if (!status.IsAlive || !isActiveAndEnabled)
            return false;

        bool available = 0 <= index && index < ItemsForSale.Count &&
               Prices[index] <= purse;

        if (available == false)
            MessageDisplay.CreateMessage("Not enough gems");
        return available;
    }

    public bool BuyItem(int purse, int index)
    {
        if(!status.IsAlive || !isActiveAndEnabled)
            return false;

        if(0 <= index && index < ItemsForSale.Count &&
           Prices[index] <= purse)
        {
            ItemsForSale.RemoveAt(index);
            Prices.RemoveAt(index);
            return true;
        }
        else
            return false;
    }
}