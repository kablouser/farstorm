﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathController : MonoBehaviour
{
    [SerializeField] private MonoBehaviour[] aliveBehaviours;
    [SerializeField] private GameObject[] aliveEffects;

    private CharacterAnimation animator;

    private void Awake()
    {
        animator = GetComponent<CharacterAnimation>();
    }

    public void SetAlive(bool isAlive, bool playAnimation = true)
    {
        if(GetComponent<Status>().IsAlive == false && isAlive == true)
            isAlive = false;

        foreach(MonoBehaviour behaviour in aliveBehaviours)
            if(behaviour)
                behaviour.enabled = isAlive;        
        foreach(GameObject gameObject in aliveEffects)
            if(gameObject)
                gameObject.SetActive(isAlive);

        Collider collider = GetComponent<Collider>();
        collider.enabled = isAlive;
        
        if(playAnimation)
        {
            // play dead animation or whatever
            if(isAlive)
            {
                animator.Play(CharacterAnimation.Parameter.idle);
            }
            else
            {
                animator.Play(CharacterAnimation.Parameter.dead);
            }
        }
        else
        {
            // return to default animation
            animator.Play(CharacterAnimation.Parameter.idle);
        }
    }
}