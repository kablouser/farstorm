﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;

[System.Serializable]
public class StatusEffect
{
    [SerializeField]
    public Resource AddResource;

    [Tooltip("Leave empty for no unique-ness.")]
    public string UniqueName;
    [Tooltip("Set to 0 for infinite duration.")]
    public float Duration;

    public StatusEffect(StatusEffect copyOf)
    {
        AddResource = copyOf.AddResource;
        UniqueName = copyOf.UniqueName;
        Duration = copyOf.Duration;
    }

    public bool IsUniqueEffect => 0 < UniqueName.Length;
    public bool IsInfiniteDuration => Duration <= 0;

    public void Multiply(float multiplier) => AddResource *= multiplier;

    public virtual Resource GetEffect
    {
        get
        {
            return AddResource;
        }
    }

    public virtual bool Applicable(List<string> uniqueEffects)
    {
        if (IsUniqueEffect)
            return uniqueEffects.Contains(UniqueName) == false;
        else
            return true;
    }

    public string ToStringAPScale(float multiplier = 0)
    {
        if (AddResource.IsZero)
            return "none";

        StringBuilder stringBuilder = new StringBuilder();
        if (IsUniqueEffect)
            stringBuilder.Append(string.Format("[Unique | {0}]\n", UniqueName));

        stringBuilder.Append("Gain ");
        if (IsInfiniteDuration == false)
            stringBuilder.Append(string.Format("For {0}s ", Duration));
        stringBuilder.Append(":");

        if(multiplier == 0)
            stringBuilder.Append(Ability.Indent("\n" + AddResource.ToStringSigned()));
        else
        {
            Resource extraFromAP = AddResource * multiplier;

            float[] baseline = AddResource.ToArray();
            float[] extra = extraFromAP.ToArray();
            string[] names = Resource.VariableNames;

            for(int i = 0; i < baseline.Length; i++)
                if (baseline[i] != 0)
                {
                    char sign = 0 <= baseline[i] ? '+' : '-';
                    stringBuilder.Append(Ability.Indent(
                        string.Format("\n{0}({1}+{2}) {3}", sign, baseline[i], Mathf.Round(extra[i]*100)/100f, names[i])));
                }
        }

        return stringBuilder.ToString();
    }

    public override string ToString()
    {
        return ToStringAPScale();        
    }
}
