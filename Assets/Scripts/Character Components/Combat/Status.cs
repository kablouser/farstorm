﻿using System.Collections.Generic;
using UnityEngine;

public class Status : MonoBehaviour
{
    private const float healthRoundoff = 1E-02f;

    private float currentHealth;
    private DeathController deathController;
    
    [SerializeField]
    [Tooltip("Readonly")]
    private Resource FinalResource;
    [SerializeField]
    [Tooltip("You can edit this")]
    private Resource BaseResourceBeforeItems;

    [SerializeField] private bool DestroyOnDeath;
    [SerializeField] private AudioClip DeathSound;
    
    private List<StatusEffect> effects;
    private List<float> effectAddedTimes;

    private bool previousAlive;

    private const float destroyDelay = 3f;

    public Resource BaseResource { get; private set; }
    public Resource GetResource { get { return FinalResource; } }

    public event System.Action<Fighter> OnHit;

    public bool IsAlive
    {
        get
        {
            return healthRoundoff < currentHealth;
        }
    }

    private void Awake()
    {
        deathController = GetComponent<DeathController>();
        effects = new List<StatusEffect>();
        effectAddedTimes = new List<float>();
        UpdateResource(false);
    }

    private void OnEnable()
    {
        previousAlive = true;
    }

    private void FixedUpdate()
    {
        UpdateResource();
    }

    private void UpdateResource(bool checkAlive = true)
    {
        Resource setFinalResource;
        Resource setBaseResource;
        setFinalResource = setBaseResource = BaseResourceBeforeItems;        

        if(effects != null)
        {
            List<string> uniqueNames = new List<string>(effects.Count);
            for(int i = 0; i < effects.Count; i++)
            {
                if (effects[i].IsInfiniteDuration == false &&
                    effectAddedTimes[i] + effects[i].Duration < Time.time)
                {
                    effects.RemoveAt(i);
                    effectAddedTimes.RemoveAt(i);
                    i--;
                }
                else if (effects[i].Applicable(uniqueNames))
                {
                    Resource addResource = effects[i].GetEffect;

                    if (effects[i].IsInfiniteDuration)
                        setBaseResource += addResource;
                    else
                        setBaseResource.health += addResource.health;

                    // Don't add health to the final resource!
                    addResource.health = 0;
                    setFinalResource += addResource;

                    if (effects[i].IsUniqueEffect)
                        uniqueNames.Add(effects[i].UniqueName);
                }
            }
        }
        BaseResource = setBaseResource;
        if (BaseResource.health < currentHealth)
            currentHealth = BaseResource.health;

        setFinalResource.health = currentHealth;
        FinalResource = setFinalResource;
        
        if (checkAlive && previousAlive != IsAlive)
        {
            if(IsAlive == false)
            {
                GetComponent<AudioSource>().PlayOneShot(DeathSound);
                if (DestroyOnDeath)   
                    Destroy(gameObject, destroyDelay);
            }
            
            deathController.SetAlive(IsAlive);
            previousAlive = IsAlive;
        }
    }

    public void Reset()
    {
        currentHealth = BaseResource.health;
        UpdateResource();        
    }

    public int AddEffect(StatusEffect effect)
    {
        effects.Add(effect);
        effectAddedTimes.Add(Time.time);
        UpdateResource();
        return effects.Count - 1;
    }

    public void RemoveEffect(StatusEffect effect)
    {
        int i = effects.IndexOf(effect);
        if(i != -1)
        {
            effects.RemoveAt(i);
            effectAddedTimes.RemoveAt(i);
            UpdateResource();
        }
    }

    public void RemoveEffect(int index)
    {
        if(0 <= index && index < effects.Count)
        {
            effects.RemoveAt(index);
            effectAddedTimes.RemoveAt(index);
        }
        UpdateResource();
    }

    public float CalculateDamage(float damage)
    {
        UpdateResource();
        damage *= 10 / (10 + Mathf.Clamp(FinalResource.armour,0,Mathf.Infinity));
        return Mathf.Clamp(damage,0,Mathf.Infinity);
    }

    public void Damage(float attack, Fighter source = null)
    {
        currentHealth -= CalculateDamage(attack);
        UpdateResource();

        if(source != null && OnHit != null)
            OnHit(source);
    }

    public void Heal(float heal)
    {
        currentHealth += heal;
        if (BaseResource.health < currentHealth)
            currentHealth = BaseResource.health;
        UpdateResource();
    }
}