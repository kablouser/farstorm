﻿using UnityEngine;
using System.Text;

[CreateAssetMenu(fileName = "Projectile Ability", menuName = "Abilities/Projectile", order = 3)]
public class ProjectileAbility : Ability
{    
    private const string rayCatcher = "RayCatcher";

    [SerializeField]
    private bool IsHoming;
    [SerializeField]
    private TargetFilter Filter;
    [SerializeField]
    private StatusModifier Modifier;

    [SerializeField]
    private Projectile ProjectilePrefab;
    [Tooltip("Set to 0 for infinite range")]
    [SerializeField]
    private float Range;

    public override ActiveMode GetActiveMode => IsHoming ? ActiveMode.Target : ActiveMode.Direction;
    public override float GetRange => Range;

    public override bool Activate(Inventory inventory, int slot)
    {
        Fighter thisFighter = inventory.GetComponent<Fighter>();

        Vector3 direction = new Vector3();
        Transform homeTarget = null;
        if(IsHoming)
        {
            if (thisFighter.AbilityTarget == null)
                return false;

            if (CheckFilter(Filter, thisFighter, thisFighter.AbilityTarget) == false)
                return false;
            else
            {
                homeTarget = thisFighter.AbilityTarget.transform;

                if (Range < Vector3.Distance(inventory.transform.position, homeTarget.transform.position))
                {
                    inventory.GetComponent<Mover>().MoveInRange(
                        homeTarget.GetComponent<Mover>(), slot, Range);
                    return false;
                }
            }
        }
        else
        {
            PlayerController player = inventory.GetComponent<PlayerController>();
            if (player)
            {
                RaycastHit[] allHits = player.BasicHit(out int hits);
                int i;
                for (i = 0; i < hits; i++)
                    if (allHits[i].collider.CompareTag(rayCatcher))
                    {
                        direction = allHits[i].point - player.transform.position;
                        direction.y = 0;
                        i = -1;
                        break;
                    }

                if (i != -1)
                    return false;
            }
            else if (thisFighter.AbilityTarget)
            {
                direction = thisFighter.AbilityTarget.transform.position - thisFighter.transform.position;
                direction.y = 0;
            }
            else
            {
                return false;
            }
        }

        var newProjectile = Instantiate(ProjectilePrefab, inventory.transform.position, 
            ProjectilePrefab.transform.rotation);

        System.Func<Collider, bool> checkCollider = collider =>
        {
            Fighter colliderFighter = collider.GetComponent<Fighter>();
            if (colliderFighter == null)
                return false;
            else return CheckFilter(Filter, thisFighter, colliderFighter);
        };

        Status status = inventory.GetComponent<Status>();
        float setRange = IsHoming ? -1 : Range;
        newProjectile.Configure(direction, checkCollider, Modifier,
            status.GetResource.abilityPower,
            status, homeTarget, setRange, -1);

        return true;
    }

    public override void Deactivate(Inventory inventory, int slot) {}

    public override string ToString()
    {        
        string startingText = IsHoming ? "Shoot [Homing] {0} Towards " : "Shoot {0} Towards ";
        startingText = string.Format(startingText, ProjectilePrefab.name);
        StringBuilder stringBuilder = new StringBuilder(startingText);

        if (Filter == TargetFilter.All)
            stringBuilder.Append("[Anyone]");
        else if (Filter == TargetFilter.FriendsOnly)
            stringBuilder.Append("[Friend]");
        else if (Filter == TargetFilter.EnemiesOnly)
            stringBuilder.Append("[Enemy]");

        string impactText = ProjectilePrefab as ExplosiveProjectile ?
            "\n\nOn Impact [Explode]:" : "\n\nOn Impact:";

        stringBuilder.Append(impactText + Indent("\n"+Modifier.ToString()));

        return stringBuilder.ToString();
    }
}