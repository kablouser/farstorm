﻿using System.Text;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "On Hit Self", menuName = "Abilities/On Hit Self", order = 1)]
public class OnHitSelf : Ability
{
    [SerializeField]
    private StatusModifier Modifier;

    private class SubscribeData
    {
        public System.Action<Fighter> EventHandler;
        public float NextActivate;

        public SubscribeData(System.Action<Fighter> eventHandler, float nextActivate)
        {
            EventHandler = eventHandler;
            NextActivate = nextActivate;
        }
    }

    private Dictionary<Status, SubscribeData> subscriptions;

    [SerializeField]
    private float Cooldown;

    private void onHit(Status status, int slot)
    {
        // assuming buff never uses slot
        if (subscriptions != null && subscriptions.ContainsKey(status) &&
            subscriptions[status].NextActivate < Time.time)
        {
            Modifier.Apply(status, status);
            subscriptions[status].NextActivate = Time.time + Cooldown;
        }
    }

    public override bool Activate(Inventory inventory, int slot)
    {
        if (subscriptions == null)
            subscriptions = new Dictionary<Status, SubscribeData>(1);

        Status status = inventory.GetComponent<Status>();
        if (subscriptions.ContainsKey(status)) return true;

        System.Action<Fighter> newEventHandler = (x) => onHit(status, slot);
        subscriptions.Add(status, new SubscribeData(newEventHandler, 0));
        inventory.GetComponent<Status>().OnHit += newEventHandler;

        return true;
    }

    public override void Deactivate(Inventory inventory, int slot)
    {
        Status status = inventory.GetComponent<Status>();
        if (subscriptions != null && subscriptions.ContainsKey(status))
        {            
            inventory.GetComponent<Status>().OnHit -= subscriptions[status].EventHandler;
            subscriptions.Remove(status);
        }
        Modifier.Remove(status);
    }

    public override string ToString()
    {
        StringBuilder stringBuilder = new StringBuilder(string.Format("When Hit ({0}s cooldown) :", Cooldown));

        string baseName = Modifier.ToString();
        if (baseName == "none")
            return "none";
        else
            stringBuilder.Append(Indent("\n"+baseName));

        return stringBuilder.ToString();
    }
}
