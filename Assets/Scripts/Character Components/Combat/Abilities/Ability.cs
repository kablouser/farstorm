﻿using UnityEngine;

public abstract class Ability : ScriptableObject
{
    public enum TargetFilter { All, FriendsOnly, EnemiesOnly };
    public enum ActiveMode { Normal, Target, Direction}
    public virtual ActiveMode GetActiveMode => ActiveMode.Normal;
    public virtual float GetRange => 0;
    public virtual TargetFilter CastFilter => TargetFilter.All;

    public abstract bool Activate(Inventory inventory, int slot);
    public abstract void Deactivate(Inventory inventory, int slot);

    public override string ToString()
    {
        return "no ability description";
    }

    public static string Indent(string input)
    {
        return input.Replace("\n", "\n    ");
    }

    public static bool CheckFilter(TargetFilter filter, Fighter caster, Fighter target) =>
        filter == TargetFilter.All ||
        filter == TargetFilter.FriendsOnly && caster.IsFriendly(target) ||
        filter == TargetFilter.EnemiesOnly && !caster.IsFriendly(target);
}