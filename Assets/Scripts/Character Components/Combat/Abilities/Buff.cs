﻿using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "Buff", menuName = "Abilities/Buff", order = 0)]
public class Buff : Ability
{
    [SerializeField]
    private StatusModifier Modifier;

    public override bool Activate(Inventory inventory, int slot)
    {
        Status status = inventory.GetComponent<Status>();
        Modifier.Apply(status, status);
        return true;
    }

    public override void Deactivate(Inventory inventory, int slot)
    {
        Modifier.Remove(inventory.GetComponent<Status>());
    }

    public override string ToString()
    {
        return Modifier.ToString();
    }
}