﻿using UnityEngine;
using System.Text;

[CreateAssetMenu(fileName = "Aura", menuName = "Abilities/Aura", order = 4)]
public class AuraAbility : Ability
{
    [SerializeField]
    private TargetFilter Filter;
    [SerializeField]
    private StatusModifier Modifier;
    [SerializeField]
    private Projectile ProjectilePrefab;
    [SerializeField]
    private float Duration;
    [SerializeField]
    private bool CenterOnTile;

    public override bool Activate(Inventory inventory, int slot)
    {
        Fighter thisFighter = inventory.GetComponent<Fighter>();
        Vector3 spawnPosition = CenterOnTile ?
            inventory.GetComponent<Mover>().CurrentTile.Position :
            inventory.transform.position;

        var newProjectile = Instantiate(ProjectilePrefab,
            spawnPosition,
            ProjectilePrefab.transform.rotation);

        System.Func<Collider, bool> checkCollider = collider =>
        {
            Fighter colliderFighter = collider.GetComponent<Fighter>();
            if (colliderFighter == null)
                return false;
            else return CheckFilter(Filter, thisFighter, colliderFighter);
        };

        Status status = inventory.GetComponent<Status>();
        newProjectile.Configure(Vector3.zero, checkCollider, Modifier,
            status.GetResource.abilityPower,
            status,null,-1,Duration);

        return true;
    }

    public override void Deactivate(Inventory inventory, int slot) { }

    public override string ToString()
    {
        StringBuilder stringBuilder = new StringBuilder("Cast Aura Affecting ");

        if (Filter == TargetFilter.All)
            stringBuilder.Append("[Anyone]");
        else if (Filter == TargetFilter.FriendsOnly)
            stringBuilder.Append("[Friend]");
        else if (Filter == TargetFilter.EnemiesOnly)
            stringBuilder.Append("[Enemy]");

        stringBuilder.Append(" Around You");

        if (0 < Duration)
        {
            stringBuilder.Append(string.Format(" For {0}s", Duration));
            stringBuilder.Append("\n\nEach Second:" + Indent("\n" + Modifier.ToString()));
        }
        else
            stringBuilder.Append("\n\nInstantly:" + Indent("\n" + Modifier.ToString()));

        return stringBuilder.ToString();
    }
}