﻿using UnityEngine;
using System.Text;
using System.Collections;

[CreateAssetMenu(fileName = "Target Ability", menuName = "Abilities/Target", order = 2)]
public class TargetAbility : Ability
{
    [SerializeField]
    private float Range;
    [SerializeField]
    private TargetFilter Filter;
    [SerializeField]
    private StatusModifier Modifier;
    [SerializeField]
    private GameObject ImpactEffect;

    public override ActiveMode GetActiveMode => ActiveMode.Target;
    public override TargetFilter CastFilter => Filter;
    public override float GetRange => Range;

    public override bool Activate(Inventory inventory, int slot)
    {
        Fighter thisFighter = inventory.GetComponent<Fighter>();
        Fighter target = thisFighter.AbilityTarget;

        if (target == null)
            return false;

        if(CheckFilter(Filter, thisFighter, target) == false)
            return false;

        if (Range < Vector3.Distance(inventory.transform.position, target.transform.position))
        {
            inventory.GetComponent<Mover>().MoveInRange(
                target.GetComponent<Mover>(), slot, Range);
            return false;
        }

        Modifier.Apply(inventory.GetComponent<Status>(), target.GetComponent<Status>());
        var newEffect = Instantiate(ImpactEffect,
            target.transform.position,
            Quaternion.LookRotation(target.transform.position - inventory.transform.position));

        return true;
    }

    public override void Deactivate(Inventory inventory, int slot) { }

    public override string ToString()
    {
        string effectString = Modifier.ToString();
        if (effectString == "none")
            return "none";

        StringBuilder stringBuilder = new StringBuilder();

        if (Filter == TargetFilter.All)
            stringBuilder.Append("Target [Anyone]");
        else if (Filter == TargetFilter.FriendsOnly)
            stringBuilder.Append("Target [Friend]");
        else if(Filter == TargetFilter.EnemiesOnly)
            stringBuilder.Append("Target [Enemy]");

        stringBuilder.Append(string.Format(" Within {0}m",Range));

        if (effectString != "none")
            stringBuilder.Append("\n\n"+effectString);
        
        return stringBuilder.ToString();
    }
}
