﻿using UnityEngine;

public class Projectile : MonoBehaviour
{    
    [SerializeField]
    protected GameObject ActiveEffect;
    [SerializeField]
    protected GameObject DisappearEffect;
    [SerializeField]
    protected float DisappearTime;
    [SerializeField]
    protected float Speed;
    protected Vector3 velocity;

    [SerializeField]
    private AudioClip CastSound;
    [SerializeField]
    private AudioClip DisappearSound;

    protected int particleID = -1;

    public bool DamageOverTime;

    [Header("Leaves these values alone pls")]
    public StatusModifier Modifier;
    public System.Func<Collider, bool> CheckCollision;
    public float StoredAbilityPower;    
    public Status Caster;
    public float DistanceLeft = -1;
    public float DurationLeft = -1;
    public Transform HomeTarget;

    public bool IsInstant;

    public Vector3 SetVelocity
    {
        set
        {
            if (0 < value.sqrMagnitude)
                velocity = value.normalized * Speed;
            else
                velocity = Vector3.zero;
        }
    }

    protected virtual void Awake()
    {
        ActiveEffect.SetActive(true);
        DisappearEffect.SetActive(false);
        particleID = Environment.Singleton.RegisterParticle(gameObject);
        GetComponent<AudioSource>().PlayOneShot(CastSound);
    }

    protected virtual void OnDestroy()
    {    
        Environment.Singleton.UnregisterParticle(particleID);
    }

    protected virtual void FixedUpdate()
    {
        if(HomeTarget)
        {
            SetVelocity = HomeTarget.position - transform.position;
            transform.LookAt(HomeTarget);
        }
        transform.Translate(velocity * Time.fixedDeltaTime, Space.World);

        if (DistanceLeft != -1)
        {
            DistanceLeft -= Speed * Time.fixedDeltaTime;
            if (DistanceLeft <= 0)
                StartDisappear();
        }
        else if (DurationLeft != -1)
        {
            DurationLeft -= Time.fixedDeltaTime;
            if (DurationLeft <= 0)
                StartDisappear();
        }
        else
        {
            Environment.Singleton.GetMapExtents(out Vector3 bottomLeft, out Vector3 topRight);
            if (transform.position.x < bottomLeft.x ||
                transform.position.z < bottomLeft.z ||
                topRight.x < transform.position.x ||
                topRight.z < transform.position.z)
            {
                StartDisappear();
            }
        }
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (DamageOverTime == false && 
           CheckCollision(other))
        {
            var target = other.GetComponent<Status>();
            Modifier.Apply(Caster, target, StoredAbilityPower);
            StartDisappear();
        }
    }

    protected virtual void OnTriggerStay(Collider other)
    {
        if (DamageOverTime == true &&
           CheckCollision(other))
        {
            var target = other.GetComponent<Status>();
            Modifier.Apply(Caster, target, StoredAbilityPower, Time.fixedDeltaTime);
        }
    }

    protected void StartDisappear()
    {
        if (enabled == false)
            return;

        GetComponent<AudioSource>().PlayOneShot(DisappearSound);
        Destroy(gameObject, DisappearTime);
        enabled = false;
        ActiveEffect.SetActive(false);
        DisappearEffect.SetActive(true);
    }

    public void Configure(
        Vector3 velocity,
        System.Func<Collider, bool> checkCollision,
        StatusModifier modifier,
        float storedAbilityPower,
        Status caster = null,       
        Transform homeTarget = null,
        float distanceLeft = -1,
        float durationLeft = -1)
    {
        SetVelocity = velocity;
        CheckCollision = checkCollision;
        Modifier = modifier;
        StoredAbilityPower = storedAbilityPower;
        Caster = caster;
        DistanceLeft = distanceLeft;
        DurationLeft = durationLeft;
        HomeTarget = homeTarget;

        if (velocity.sqrMagnitude != 0)
            transform.LookAt(transform.position + this.velocity);
        else if(HomeTarget != null)
            transform.LookAt(HomeTarget);
    }
}