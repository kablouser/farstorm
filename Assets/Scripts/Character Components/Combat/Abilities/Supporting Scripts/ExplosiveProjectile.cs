﻿using UnityEngine;

public class ExplosiveProjectile : Projectile
{
    private static LayerMask explodeMask;

    [Header("Explosive Parameters")]
    [SerializeField]
    protected GameObject ExplosionParticle;
    [SerializeField]
    protected float ExplosionDuration;
    [SerializeField]
    protected float ExplosionRadius;

    [SerializeField]
    private AudioClip ExplodeSound;

    protected override void Awake()
    {
        base.Awake();
        ExplosionParticle.SetActive(false);
        explodeMask = LayerMask.GetMask("Clickable");
    }

    protected virtual void ExplodeEffect()
    {
        Destroy(gameObject, ExplosionDuration);
        enabled = false;
        ActiveEffect.SetActive(false);
        DisappearEffect.SetActive(false);
        ExplosionParticle.SetActive(true);

        GetComponent<AudioSource>().PlayOneShot(ExplodeSound);
    }

    protected override void OnTriggerEnter(Collider other)
    {
        if (DamageOverTime == false &&
           CheckCollision(other))
        {
            Collider[] results = Physics.OverlapSphere(transform.position, ExplosionRadius, explodeMask);
            foreach (var collider in results)
                if(CheckCollision(collider))
                    Modifier.Apply(Caster, collider.GetComponent<Status>(), StoredAbilityPower);

            ExplodeEffect();
        }
    }
}