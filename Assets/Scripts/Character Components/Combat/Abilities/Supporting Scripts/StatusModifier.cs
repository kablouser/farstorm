﻿using UnityEngine;
using System.Text;

[System.Serializable]
public class StatusModifier
{
    [SerializeField]
    private float Heal;
    [SerializeField]
    private float Damage;
    [SerializeField]
    private StatusEffect Effect;
    [SerializeField]
    private float AbilityPowerScaling;

    private bool Removable => Effect.IsInfiniteDuration;

    public void InternalApply(Status fromStatus, Status targetStatus, float multiplier)
    {
        if (Heal != 0)
            targetStatus.Heal(Heal * multiplier);
        if (Damage != 0)
            targetStatus.Damage(Damage * multiplier, fromStatus.GetComponent<Fighter>());
        if (Effect.ToString() != "none")
        {
            if(Removable)
            {
                targetStatus.AddEffect(Effect);
            }
            else
            {
                StatusEffect newEffect = new StatusEffect(Effect);
                newEffect.Multiply(multiplier);
                targetStatus.AddEffect(newEffect);
            }
        }
    }

    public void Apply(Status fromStatus, Status targetStatus)
    {
        Apply(fromStatus, targetStatus, fromStatus.GetResource.abilityPower);
    }

    public void Apply(Status fromStatus, Status targetStatus, float abilityPower)
    {
        float multiplier = 1 + abilityPower * AbilityPowerScaling;
        InternalApply(fromStatus, targetStatus, multiplier);
    }

    public void Apply(Status fromStatus, Status targetStatus, float abilityPower, float multiplier)
    {
        multiplier = (1 + abilityPower * AbilityPowerScaling) * multiplier;
        InternalApply(fromStatus, targetStatus, multiplier);
    }

    public void Remove(Status thisStatus)
    {
        if (Removable)
        {
            thisStatus.RemoveEffect(Effect);
        }   
    }

    public override string ToString()
    {
        StringBuilder stringBuilder = new StringBuilder();

        float multiplier = AbilityPowerScaling *
            PlayerController.Singleton.GetComponent<Status>().GetResource.abilityPower;

        if (Heal != 0)
        {
            if (0 == multiplier)
                stringBuilder.Append(string.Format("Restore {0} Health", Heal));
            else
                stringBuilder.Append(string.Format("Restore {0}(+{1}) Health", 
                    Heal, Mathf.Round(multiplier * Heal * 100)/100f));
        }
        if (Damage != 0)
        {
            if (0 < stringBuilder.Length)
                stringBuilder.Append("\n");

            if (0 == multiplier)
                stringBuilder.Append(string.Format("Deal {0} Damage", Damage));
            else
                stringBuilder.Append(string.Format("Deal {0}(+{1}) Damage", 
                    Damage, Mathf.Round(multiplier * Damage * 100) / 100f));
        }

        string effectString = Removable ? Effect.ToString() : Effect.ToStringAPScale(multiplier);
        if (effectString != "none")
        {
            if (0 < stringBuilder.Length)
                stringBuilder.Append("\n\n");
            stringBuilder.Append(effectString);
        }

        if (0 == stringBuilder.Length)
            return "none";
        else
            return stringBuilder.ToString();
    }
}
