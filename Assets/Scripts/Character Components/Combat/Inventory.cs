﻿using UnityEngine;

public class Inventory : MonoBehaviour
{
    [SerializeField] private ItemInstance[] Items = new ItemInstance[NumberOfSlots];

    public const int NumberOfSlots = 6;
    public ItemInstance[] GetItems { get { return Items; } }

    public event System.Action<int> OnSlotChanged;

    private Status status;

    private void Awake()
    {
        status = GetComponent<Status>();
    }

    private void Start()
    {
        status.Reset();

        for (int i = 0; i < NumberOfSlots; i++)
            if(Items[i].Item)
                Items[i].Item.OnEquip(this, i);
    }

    private void CheckSlot(int slot)
    {
        if(slot < 0 || NumberOfSlots <= slot)
            throw new System.ArgumentOutOfRangeException("slot number is out of range");
    }

    // Assume slot is valid
    private void ChangeInventory(int slot, ItemInstance setItem)
    {
        if(setItem == null)
            Items[slot] = new ItemInstance(null);
        else
            Items[slot] = setItem;

        OnSlotChanged?.Invoke(slot);
    }

    public ItemInstance AddItem(int slot, ItemInstance newItem)
    {
        CheckSlot(slot);
        
        ItemInstance currentItem = RemoveItem(slot);

        ChangeInventory(slot, newItem);

        if(newItem != null && newItem.Item)
            newItem.Item.OnEquip(this, slot);

        return currentItem;
    }

    public bool AddItem(ItemInstance newItem, bool playMessages = true)
    {
        // find empty slot
        int i = IndexOf(null);
        if (i != -1)
        {
            ChangeInventory(i, newItem);
            if (newItem != null && newItem.Item)
                newItem.Item.OnEquip(this, i);

            return true;
        }
        else
        {
            if (playMessages && GetComponent<PlayerController>())
                MessageDisplay.CreateMessage("Inventory is full");
            return false;
        }
    }

    // returns index of item, -1 if not found
    public int RemoveItem(Item item)
    {
        int i = IndexOf(item);
        
        if(i != -1)
        {
            //items contains item!
            RemoveItem(i);
        }
        
        return i;
    }

    public ItemInstance RemoveItem(int slot)
    {
        CheckSlot(slot);

        ItemInstance removedItem = null;

        if(Items[slot].Item)
        {
            Items[slot].Item.OnUnequip(this, slot);
            removedItem = Items[slot];
            ChangeInventory(slot, null);
        }
        
        return removedItem;
    }

    // returns index of item, -1 if not found
    public int IndexOf(Item item)
    {
        int i = 0;
        for(; i < NumberOfSlots; i++)
            if(Items[i].Item == item)
                break;

        return i < NumberOfSlots ? i : -1;
    }

    public bool UseItem(int slot)
    {
        if(status.IsAlive == false)
            return false;

        CheckSlot(slot);

        if(Items[slot].Item)
        {
            var active = Items[slot].Item.Active;
            bool isPlayer = GetComponent<PlayerController>();

            if (active == null)
                return false;
            else if (isPlayer)
            {
                var thisFighter = GetComponent<Fighter>();
                if (Ability.CheckFilter(
                    active.CastFilter,
                    thisFighter, thisFighter.AbilityTarget) == false)
                    MessageDisplay.CreateMessage(active.CastFilter);
            }

            if (Items[slot].CooldownTimer < Time.time)
            {
                if (Items[slot].Item.UseActive(this, slot))
                {
                    if (Items[slot].Item)
                        Items[slot].CooldownTimer = Time.time + Items[slot].Item.ActiveCooldown;
                    return true;
                }
            }
            else if(isPlayer)
            {
                MessageDisplay.CreateMessage("Item is on cooldown");
            }
        }

        return false;
    }

    public float GetCooldown(int slot)
    {
        CheckSlot(slot);
        return Items[slot].CooldownTimer;
    }

    public void ClearInventory()
    {
        for(int i = 0; i < NumberOfSlots; i++)
            RemoveItem(i);
    }
}