﻿[System.Serializable]
public class ItemInstance
{
    public Item Item;
    public float CooldownTimer;

    public ItemInstance(Item item)
    {
        Item = item;
        CooldownTimer = 0;
    }
}
