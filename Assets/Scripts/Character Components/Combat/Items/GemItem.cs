﻿using UnityEngine;

[CreateAssetMenu(fileName = "Gem Item", menuName = "Items/Gem Item", order = 2)]
public class GemItem : Item
{
    public float Average;
    public float Variation;

    public override void OnEquip(Inventory inventory, int slot)
    {
        PlayerController player = inventory.GetComponent<PlayerController>();

        if(player != null)
        {
            int randomGems = Mathf.RoundToInt(Random.Range(Average - Variation, Average + Variation));
            if (randomGems <= 0)
                randomGems = 1;

            player.GemsPurse += randomGems;
            player.CoinSoundPlayer.PlaySound();
            inventory.RemoveItem(slot);
        }
        else if(Passive)
            Passive.Activate(inventory,slot);
    }

    public override void OnUnequip(Inventory inventory, int slot)
    {
        if(Passive)
            Passive.Deactivate(inventory, slot);
        if(Active)
            Active.Deactivate(inventory, slot);
    }

    public static bool IsGemItem(Item testItem) =>
        testItem as GemItem != null;
}
