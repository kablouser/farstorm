﻿using UnityEngine;
using System.Text;

[CreateAssetMenu(fileName = "Item", menuName = "Items/Default Item", order = 1)]
public class Item : ScriptableObject
{
    public Sprite Display;
    public Ability Passive;
    public Ability Active;
    public float ActiveCooldown;
    public bool Consumable;

    public virtual void OnEquip(Inventory inventory, int slot)
    {
        if(Passive)
            Passive.Activate(inventory, slot);
    }

    public virtual void OnUnequip(Inventory inventory, int slot)
    {
        if(Passive)
            Passive.Deactivate(inventory, slot);
        if(Active)
            Active.Deactivate(inventory, slot);
    }
    
    public virtual bool UseActive(Inventory inventory, int slot)
    {
        if (Active)
        {
            bool success = Active.Activate(inventory, slot);
            if (success && Consumable)
                inventory.RemoveItem(slot);

            return success;
        }
        else return false;
    }

    public override string ToString()
    {
        StringBuilder stringBuilder = new StringBuilder(name);
        if(Passive != null)
        {
            stringBuilder.Append("\n\n" + Passive.ToString());
        }

        if(Active != null)
        {
            if (Consumable)
                stringBuilder.Append("\n\nClick to Activate (1 use) :");
            else
                stringBuilder.Append(
                    string.Format("\n\nClick to Activate ({0}s cooldown) :",
                    Mathf.Round(ActiveCooldown*100)/100f
                    ));

            stringBuilder.Append(Ability.Indent("\n"+Active.ToString()));
        }

        return stringBuilder.ToString();
    }
}