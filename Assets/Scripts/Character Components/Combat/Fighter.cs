﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Fighter : MonoBehaviour
{
    // Larger means lots of health will show even larger health bars!
    private const float healthBarScale = .2f;
    private const float damageApplicationTime = .417f;
    private const float unlockTime = .5f;

    private static readonly float ln2 = Mathf.Log(2);

    [SerializeField] private Race FactionRace;
    [SerializeField] private AudioClip AttackSound;
    [SerializeField] private AudioSource HitSource;

    private float nextAttack;

    private Mover mover;
    private CharacterAnimation animator;
    private Slider healthBar;
    
    private Status status;

    private Coroutine damageFunction;

    public enum Race { Human, Orc, Robot };

    public Fighter CurrentTarget {get; private set;}
    public Fighter AbilityTarget;

    private void Awake()
    {
        mover = GetComponent<Mover>();
        animator = GetComponent<CharacterAnimation>();
        healthBar = GetComponentInChildren<Slider>(true);
        status = GetComponent<Status>();
    }

    private void OnEnable()
    {
        CurrentTarget = null;
    }

    private void OnDisable()
    {
        if (damageFunction != null)
        {
            StopCoroutine(damageFunction);
            mover.IsLocked = false;
        }
    }

    private void FixedUpdate()
    {
        // check for null
        if(CurrentTarget)
        {
            // check our mover is moving to our target
            if(mover.FollowMover == CurrentTarget.mover)
                TryAttack(CurrentTarget);
            else
                CurrentTarget = null;
        }

        healthBar.transform.localScale = Vector3.one * Mathf.Sqrt(healthBarScale * status.BaseResource.health);
        healthBar.value = status.GetResource.health / status.BaseResource.health;
    }

    private IEnumerator ApplyDamage(float attackCooldown, Status target)
    {
        yield return new WaitForSeconds(damageApplicationTime * attackCooldown);
        target.Damage(status.GetResource.attack, this);
        HitSource.PlayOneShot(AttackSound);
        yield return new WaitForSeconds((unlockTime - damageApplicationTime) * attackCooldown);
        mover.IsLocked = false;
    }

    public bool InRange(Fighter other)
    {
        float distance = Vector3.Distance(other.transform.position,
                                          transform.position);
        return distance <= status.GetResource.attackRange;
    }

    public void TryAttack(Fighter other)
    {
        if(other.status.IsAlive == false || IsFriendly(other))
        {
            CurrentTarget = null;
            return;
        }

        if(other != CurrentTarget)
        {
            // switch to new target
            CurrentTarget = other;
            mover.Stop();
            mover.Follow(other.mover);
        }

        if(nextAttack < Time.time && other != this && InRange(other))
        {
            // attack speed calculation
            // y = 2^{-(x-1)}
            float cooldown = Mathf.Exp(-ln2*(status.GetResource.attackSpeed - 1));
            nextAttack = Time.time + cooldown;

            // stop and lock mover
            mover.Stop(false);
            mover.IsLocked = true;

            transform.LookAt(other.transform.position, Vector3.up);

            animator.Play(CharacterAnimation.Parameter.attack);
            animator.SetSpeed(CharacterAnimation.SpeedParam.attackSpeed, 1/cooldown);

            if (damageFunction != null)
                StopCoroutine(damageFunction);
            damageFunction = StartCoroutine(ApplyDamage(cooldown, other.status));
        }
    }

    public void StopAttack()
    {
        if(CurrentTarget != null)
        {
            mover.Stop();
            CurrentTarget = null;            
        }
    }

    public bool IsFriendly(Fighter other)
    {
        return FactionRace == other.FactionRace;
    }
}