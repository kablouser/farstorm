﻿using System;
using System.Text;

[Serializable]
public struct Resource
{
    public float
    health,
    movementSpeed,
    attack,
    attackSpeed,
    attackRange,
    armour,
    abilityPower;

    public float[] ToArray()
    {
        return new float[]
        {
            health,
            movementSpeed,
            attack,
            attackSpeed,
            attackRange,
            armour,
            abilityPower
        };
    }

    public static string[] VariableNames
    {
        get
        {
            return new string[]
            {
                "health",
                "movement speed",
                "attack",
                "attack speed",
                "attack range",
                "armour",
                "ability power"
            };
        }
    }

    public Resource(float health, 
        float movementSpeed, 
        float attack, 
        float attackSpeed, 
        float attackRange, 
        float armour, 
        float abilityPower)
    {
        this.health = health;
        this.movementSpeed = movementSpeed;
        this.attack = attack;
        this.attackSpeed = attackSpeed;
        this.attackRange = attackRange;
        this.armour = armour;
        this.abilityPower = abilityPower;
    }

    public bool IsZero
    {
        get
        {
            return health == 0 && movementSpeed == 0 && attack == 0 &&
                attackSpeed == 0 && attackRange == 0 && armour == 0 && abilityPower == 0;
        }
    }

    public static Resource operator +(Resource a) => a;
    public static Resource operator -(Resource a) => 
        new Resource(-a.health, -a.movementSpeed, -a.attack, -a.attackSpeed, -a.attackRange, -a.armour, -a.abilityPower);

    public static Resource operator +(Resource a, Resource b) =>
        new Resource(
            a.health + b.health, 
            a.movementSpeed + b.movementSpeed, 
            a.attack + b.attack, 
            a.attackSpeed + b.attackSpeed, 
            a.attackRange + b.attackRange,
            a.armour + b.armour,
            a.abilityPower + b.abilityPower);

    public static Resource operator -(Resource a, Resource b) => a + (-b);

    public static Resource operator *(float multiplier, Resource a) => 
        new Resource(
            a.health * multiplier, 
            a.movementSpeed * multiplier, 
            a.attack * multiplier, 
            a.attackSpeed * multiplier, 
            a.attackRange * multiplier,
            a.armour * multiplier,
            a.abilityPower * multiplier);

    public static Resource operator *(Resource a, float multiplier) => multiplier * a;

    public static Resource operator /(Resource a, float multiplier) => a * (1/multiplier);

    public override string ToString()
    {
        return $"health:{health}\n"+
            $"movement speed:{movementSpeed}\n"+
            $"attack:{attack}\n"+
            $"attack speed:{attackSpeed}\n"+
            $"attack range:{attackRange}\n"+
            $"armour:{armour}\n"+
            $"ability power:{abilityPower}";
    }
    /*
     *     health,
    movementSpeed,
    attack,
    attackSpeed,
    attackRange,
    armour,
    abilityPower;
     */
    public string ToStringSigned()
    {
        StringBuilder stringBuilder = new StringBuilder();

        float[] arrayForm = ToArray();
        string[] names = VariableNames;
        for(int i = 0; i < arrayForm.Length; i++)
            if (arrayForm[i] != 0)
            {
                string signedNumber = 0 < arrayForm[i] ? "+"+arrayForm[i].ToString() : arrayForm[i].ToString();

                if (0 < stringBuilder.Length)
                    stringBuilder.Append("\n");
                stringBuilder.Append(string.Format("{0} {1}",
                    signedNumber, names[i]));
            }

        if (stringBuilder.Length == 0)
            stringBuilder.Append("none");

        return stringBuilder.ToString();
    }
}