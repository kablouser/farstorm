﻿using UnityEngine;

public class PathHighlight : MonoBehaviour
{
    private static readonly Vector3 pathOffset = new Vector3(0, 0.1f);

    [SerializeField]
    private LineRenderer LineRenderer;
    private Mover playerMover;

    private void OnEnable()
    {
        if(playerMover == null)    
            playerMover = GetComponent<Mover>();
        playerMover.OnRouteChanged += PathChanged;
        PathChanged(null);
    }

    private void OnDisable()
    {
        // VERY IMPORTANT
        playerMover.OnRouteChanged -= PathChanged;
    }

    private void FixedUpdate()
    {
        if(LineRenderer.enabled)
            LineRenderer.SetPosition(0, transform.position + pathOffset);
    }

    private void PathChanged(EnvironmentTile[] path)
    {
        if (path == null || path.Length == 0)
        {
            LineRenderer.enabled = false;
        }
        else
        {
            LineRenderer.enabled = true;
            LineRenderer.positionCount = path.Length + 1;
            Vector3[] points = new Vector3[path.Length + 1];
            points[0] = transform.position + pathOffset;
            for(int i = 0; i < path.Length; i++)
            {
                points[i + 1] = path[i].Position + pathOffset;
            }
            LineRenderer.SetPositions(points);
        }
    }
}