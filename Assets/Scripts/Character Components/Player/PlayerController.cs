﻿using UnityEngine;

using static InputStrings;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float clickRayDistance = 100f;
    [SerializeField] private string clickLayerName = "Clickable";

    private LayerMask clickRayMask;

    private Mover mover;
    private Fighter fighter;

    private const int NumberOfRaycastHits = 8;
    private RaycastHit[] RaycastHits;

    private static readonly KeyCode[] abilityKeys = new KeyCode[6] 
    {KeyCode.Q, KeyCode.W, KeyCode.E, KeyCode.R, KeyCode.T, KeyCode.Y};
    
    public int GemsPurse;
    public ClipCollection CoinSoundPlayer;

    public static PlayerController Singleton;

    private void Awake()
    {
        Singleton = this;
        
        mover = GetComponent<Mover>();
        fighter = GetComponent<Fighter>();

        RaycastHits = new RaycastHit[NumberOfRaycastHits];
        
        clickRayMask = LayerMask.GetMask(clickLayerName);
    }

    private void Update()
    {
        if (Input.GetButtonDown(Fire2))
        {
            GetHitObject(out Object hitObject);
            Fighter hitFighter = hitObject as Fighter;
            EnvironmentTile hitTile = hitObject as EnvironmentTile;
            if (hitFighter != null)
                fighter.TryAttack(hitFighter);
            else if(hitTile != null)
                mover.GoTo(hitTile);
        }

        for (int i = 0; i < abilityKeys.Length; i++)
            if (Input.GetKeyDown(abilityKeys[i]))
                PlayerInventoryConnector.Singleton.OnItemClick(i);
    }
    
    private int ClickRaycast()
    {
        Ray screenClick = Camera.main.ScreenPointToRay(Input.mousePosition);
        int hits = Physics.RaycastNonAlloc(
            screenClick, 
            RaycastHits, 
            clickRayDistance, 
            clickRayMask);
        return hits;
    }

    public RaycastHit[] BasicHit(out int hits)
    {
        hits = ClickRaycast();
        return (RaycastHit[]) RaycastHits.Clone();
    }

    public void GetHitObject(out Object hitObject)
    {
        float currentDistance = Mathf.Infinity;
        hitObject = null;
        int hits = ClickRaycast();
        for(int i = 0; i < hits; i++)
        {
            float hitDistance = RaycastHits[i].distance;

            if(hitDistance < currentDistance)
            {
                var raycastHit = RaycastHits[i].transform;
                Fighter hitFighter = raycastHit.GetComponent<Fighter>();
                EnvironmentTile hitTile = raycastHit.GetComponent<EnvironmentTile>();

                if (hitFighter &&
                    hitFighter.GetComponent<Status>().IsAlive)
                {
                    hitObject = hitFighter;
                    currentDistance = hitDistance;
                }
                else if (hitTile)
                {
                    hitObject = hitTile;
                    currentDistance = hitDistance;
                }                
            }
        }
    }
}
