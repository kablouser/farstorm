﻿using UnityEngine;

public class CameraControls : MonoBehaviour
{
    [SerializeField] private float PanSpeed = 7.0f;
    [SerializeField] private int ScreenEdgeWidth = 50;
    // this translation moves the camera into viewing any object
    [SerializeField] private Vector3 ViewTranslation = new Vector3(0, 19, -30);

    private Environment environment;
    private PlayerController player;
    private Quaternion constantRotation;

    public static CameraControls Singleton;
    
    private void Awake()
    {
        Singleton = this;
        environment = Environment.Singleton;
        constantRotation = transform.rotation;
    }

    private void OnEnable()
    {
        Cursor.lockState = CursorLockMode.Confined;
        transform.rotation = constantRotation;
    }

    private void OnDisable()
    {
        Cursor.lockState = CursorLockMode.None;
    }

    private bool InRange(float x, int minimum, int maximum)
    {
        return minimum <= x && x <= maximum;
    }

    private void Update()
    {
        Vector3 cameraMovement = Vector3.zero;

        if(InRange(Input.mousePosition.x, 0, ScreenEdgeWidth))
            cameraMovement.x = -1;
        else if(InRange(Input.mousePosition.x, Screen.width - ScreenEdgeWidth, Screen.width))
            cameraMovement.x = 1;
        
        if(InRange(Input.mousePosition.y, 0, ScreenEdgeWidth))
            cameraMovement.z = -1;
        else if(InRange(Input.mousePosition.y, Screen.height - ScreenEdgeWidth, Screen.height))
            cameraMovement.z = 1;
        
        transform.Translate(cameraMovement * PanSpeed * Time.deltaTime, Space.World);

        if(environment.GetMapExtents(out Vector3 bottomLeftLimit, out Vector3 topRightLimit))
        {
            Vector3 newPosition = transform.position;

            bottomLeftLimit += ViewTranslation;
            topRightLimit += ViewTranslation;

            newPosition.x = Mathf.Clamp(transform.position.x, bottomLeftLimit.x, topRightLimit.x);
            newPosition.z = Mathf.Clamp(transform.position.z, bottomLeftLimit.z, topRightLimit.z);
            transform.position = newPosition;
        }
    }

    public void Focus(Vector3 position)
    {
        transform.position = position + ViewTranslation;
    }
}