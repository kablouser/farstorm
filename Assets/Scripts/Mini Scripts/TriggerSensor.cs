﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class TriggerSensor : MonoBehaviour
{
    public System.Action<Collider> Sensed;

    private void OnTriggerEnter(Collider other)
    { 
        if(Sensed != null) Sensed(other);
    }

    private void OnTriggerStay(Collider other)
    {
        if(Sensed != null) Sensed(other);
    }
}
