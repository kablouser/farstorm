﻿using UnityEngine;

public class MusicPlaylist : MonoBehaviour
{
    [SerializeField] private AudioClip[] music;
    [SerializeField] private float timeBetween;

    private AudioSource source;

    private void Awake()
    {
        source = GetComponent<AudioSource>();
    }
    private void FixedUpdate()
    {
        if(source.isPlaying == false)
        {
            AudioClip randomClip;
            while ((randomClip = music[Random.Range(0, music.Length)]) == source.clip) ;

            source.clip = randomClip;
            source.PlayDelayed(timeBetween);
        }
    }
}
