﻿using UnityEngine;

public class SimpleParticle : MonoBehaviour
{
    [SerializeField]
    private float Duration;
    private int id = -1;

    private void Awake()
    {
        id = Environment.Singleton.RegisterParticle(gameObject);
        GetComponent<AudioSource>().Play();
        Destroy(gameObject, Duration);        
    }
    private void OnDestroy()
    {
        Environment.Singleton.UnregisterParticle(id);
    }
}
