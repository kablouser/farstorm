﻿using UnityEngine;

public class RangeIndicator : MonoBehaviour
{
    [SerializeField]
    [Range(0.001f, 1f)]
    private float SegmentsPerUnit;
    
    private LineRenderer lineRenderer;
    private SpriteRenderer spriteRenderer;
    
    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    public void SetRange(float range)
    {
        gameObject.SetActive(true);

        int segments = Mathf.CeilToInt(2 * Mathf.PI * range / SegmentsPerUnit);
        Vector3[] points = new Vector3[segments];
        float segmentAngle = 2 * Mathf.PI / segments;

        for (int i = 0; i < points.Length; i++)
        {
            points[i].x = range * Mathf.Sin(i * segmentAngle);
            points[i].z = range * Mathf.Cos(i * segmentAngle);
        }

        lineRenderer.positionCount = points.Length;
        lineRenderer.SetPositions(points);

        spriteRenderer.size = new Vector2(range*2, range*2);
    }

    public void TurnOff()
    {
        gameObject.SetActive(false);
    }
}