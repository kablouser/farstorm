﻿public static class InputStrings
{
    // using cached strings is better than creating new string objects every frame
    public static string
    Fire1 = "Fire1",
    Fire2 = "Fire2",
    MouseX = "MouseX",
    Cancel = "Cancel";
}