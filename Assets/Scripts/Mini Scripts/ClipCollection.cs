﻿using UnityEngine;

public class ClipCollection : MonoBehaviour
{
    [SerializeField]
    private AudioClip[] Footsteps;
    [SerializeField]
    private AudioSource Source;
    public void PlaySound()
    {
        Source.PlayOneShot(Footsteps[Random.Range(0, Footsteps.Length)]);
    }
}
