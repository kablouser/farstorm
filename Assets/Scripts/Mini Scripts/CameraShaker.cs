﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShaker : MonoBehaviour
{
    Vector2 sum;
    Queue<Vector2> queue;
    Queue<Vector2> requestQueue;

    [SerializeField]
    private float ReturnSpeed = 2f;


    [Tooltip("Higher is sharper")]
    public float Force;
    [Tooltip("Higher is smoother")]
    public int Ordinal;
    public float ShakeAngle;
    public float Duration;

    public static CameraShaker Singleton;

    private void Awake()
    {
        sum = Vector2.zero;
        queue = new Queue<Vector2>();
        requestQueue = new Queue<Vector2>();
        Singleton = this;
    }

    private void Update()
    {
        if (0 < Duration)
        {
            while (0 < Ordinal && queue.Count < Ordinal)
            {
                var add = Random.insideUnitCircle;
                sum += add;
                queue.Enqueue(add);
            }

            Vector3 randomPoint = sum / queue.Count * Force * Time.deltaTime;
            transform.localPosition = randomPoint;
            transform.localRotation = Quaternion.Euler(0, 0, randomPoint.x * ShakeAngle);

            while (0 < Ordinal && Ordinal <= queue.Count)
            {
                sum -= queue.Dequeue();
            }

            var newTerm = Random.insideUnitCircle;
            queue.Enqueue(newTerm);
            sum += newTerm;

            Duration -= Time.deltaTime;
        }
        else
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, Time.deltaTime * ReturnSpeed);
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.identity, Time.deltaTime * ReturnSpeed);
        }
    }

    public void Shake(float power, float duration)
    {
        if (Force < power)
        {


            Force = power;
            Ordinal = Mathf.CeilToInt(power / 2);
            ShakeAngle = power * 0.6f;
            Duration = duration;
        }
    }
}
