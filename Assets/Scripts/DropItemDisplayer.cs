﻿using System.Collections.Generic;
using UnityEngine;

public class DropItemDisplayer : MonoBehaviour
{
    [SerializeField] private Sprite MultipleItemIcon;
    [SerializeField] private GameObject GemModelPrefab;

    public static DropItemDisplayer Singleton;

    private void Awake()
    {
        Singleton = this;
    }

    public void UpdateTileDisplay(EnvironmentTile tile)
    {
        List<ItemInstance> tileItems = tile.DroppedItems;

        SpriteRenderer dropItemDisplay = tile.GetComponentInChildren<SpriteRenderer>(true);
        GameObject parentObject = dropItemDisplay.transform.parent.gameObject;
        int totalItems = tileItems.Count;

        bool isGem = false;

        foreach(ItemInstance item in tileItems)
            if(GemItem.IsGemItem(item.Item))
            {
                isGem = true;
                totalItems--;
            }
        
        GameObject gemModel = tile.ModelDisplay;

        if(isGem)
        {
            if(gemModel == null)
            {
                gemModel = Instantiate(GemModelPrefab,
                    tile.transform.position,
                    GemModelPrefab.transform.rotation,
                    tile.transform.GetChild(0));
                gemModel.transform.Translate(GemModelPrefab.transform.position,Space.Self);
                
                tile.ModelDisplay = gemModel;
            }
        }
        else
        {
            if(gemModel != null)
            {
                gemModel.transform.parent = null;
                Destroy(gemModel);
                tile.ModelDisplay = null;
            }
        }

        if(1 < totalItems)
        {
            parentObject.SetActive(true);
            dropItemDisplay.sprite = MultipleItemIcon;
        }
        else if(1 == totalItems)
        {
            parentObject.SetActive(true);
            for (int i = 0; i < tileItems.Count; i++)
                if (GemItem.IsGemItem(tileItems[i].Item) == false)
                {
                    dropItemDisplay.sprite = tileItems[i].Item.Display;
                    break;
                }
        }
        else
        {
            parentObject.SetActive(false);
        }
    }
}