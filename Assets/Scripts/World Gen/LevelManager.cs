﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelManager : MonoBehaviour
{
    [System.Serializable]
    public class EntitySpawn
    {
        public GameObject Entity;
        public float Average;
        public int MinLevel;
        [Range(0,1)]
        public float AverageGrowPercent;
        public Vector2 SpawnRange;
    }

    [SerializeField] private int Level;
    [SerializeField] private Vector2Int BaseLevelSize = new Vector2Int(20, 16);
    [SerializeField] private int AreaGrow = 32;
    [SerializeField] private List<EntitySpawn> Friends;
    [SerializeField] private List<EntitySpawn> Enemies;
    [SerializeField] private TextMeshProUGUI LevelDisplay;

    private Environment environment;
    private Vector2Int currentLevelSize;

    public int GetLevel { get { return Level; } }

    private void Awake()
    {
        environment = GetComponentInChildren<Environment>();
    }

    private void CalculateLevelSize()
    {
        float extraArea = (Level - 1) * AreaGrow;

        currentLevelSize.x = Mathf.CeilToInt(Mathf.Sqrt(
            BaseLevelSize.x * (BaseLevelSize.x + extraArea / BaseLevelSize.y)));

        currentLevelSize.y = Mathf.CeilToInt(Mathf.Sqrt(
            BaseLevelSize.y * (BaseLevelSize.y + extraArea / BaseLevelSize.x)));
    }

    private bool TryAddEntity(EntitySpawn entity, out Environment.SpawnData spawn)
    {
        if (entity.MinLevel <= GetLevel)
        {
            float extraAreaRatio = (Level - entity.MinLevel) * AreaGrow / (BaseLevelSize.x * BaseLevelSize.y);

            //currentLevelSize.y / 2, currentLevelSize.y - 2, 
            spawn = new Environment.SpawnData(entity.Entity,
                entity.Average + entity.AverageGrowPercent * extraAreaRatio,
                Mathf.RoundToInt(currentLevelSize.y * entity.SpawnRange.x),
                Mathf.RoundToInt(currentLevelSize.y * entity.SpawnRange.y));
            return true;
        }
        else
        {
            spawn = null;
            return false;
        }
    }

    private void ConfigEnvironment()
    {
        CalculateLevelSize();

        List<Environment.SpawnData> allChances = new List<Environment.SpawnData>();
        foreach (var friend in Friends)
            if(TryAddEntity(friend, out Environment.SpawnData spawn))
                allChances.Add(spawn);
        foreach(var enemy in Enemies)
            if (TryAddEntity(enemy, out Environment.SpawnData spawn))
                allChances.Add(spawn);
        
        Vector2Int playerStart = new Vector2Int();
        playerStart.x = Mathf.RoundToInt(currentLevelSize.x * Random.Range(0.3f,0.7f));        
        playerStart.y = Mathf.RoundToInt(currentLevelSize.y * Random.Range(0f,0.2f));
        // border control
        playerStart.x = Mathf.Clamp(playerStart.x, 1, currentLevelSize.x - 2);
        playerStart.y = Mathf.Clamp(playerStart.y, 1, currentLevelSize.y - 2);

        environment.CleanUpWorld();
        environment.ConfigureWorld(currentLevelSize, allChances, playerStart);
        environment.GenerateWorld();
    }

    private void UpdateLevelDisplay()
    {
        LevelDisplay.SetText("<size=24>level</size> "+GetLevel.ToString());
    }

    public void ResetGame()
    {
        Level = 1;
        UpdateLevelDisplay();
        ConfigEnvironment();
    }

    public void NextLevel()
    {
        Level++;
        UpdateLevelDisplay();
        ConfigEnvironment();
    }
}