﻿using System.Collections.Generic;
using UnityEngine;

public class EnvironmentTile : MonoBehaviour
{
    // Pathfinding data
    public EnvironmentTile Parent { get; set; }
    public Vector3 Position { get; set; }
    public float Global { get; set; }
    public float Local { get; set; }
    public bool Visited { get; set; }

    // DONT APPEND TO THIS
    public List<EnvironmentTile> Connections;    
    public bool IsAccessible;
    public Mover Occupier;
    public List<ItemInstance> DroppedItems;
    public GameObject ModelDisplay;

    public bool IsAdjacent(EnvironmentTile other)
    {
        return Connections.Contains(other);
    }

    public bool TryOccupy(Mover newOccupier)
    {
        if(Occupier || IsAccessible == false)
            return false;
        else
        {
            Occupier = newOccupier;
            if(Occupier.CurrentTile)
            {
                Occupier.CurrentTile.Occupier = null;
            }
            Occupier.CurrentTile = this;
            return true;
        }
    }

    public void DropItem(ItemInstance dropItem)
    {
        if(DroppedItems == null)
            DroppedItems = new List<ItemInstance>() {dropItem};
        else
            DroppedItems.Add(dropItem);

        DropItemDisplayer.Singleton.UpdateTileDisplay(this);
    }

    public bool PickupItem(ItemInstance pickItem)
    {
        bool success;
        if(DroppedItems == null)
            success = false;
        else
            success = DroppedItems.Remove(pickItem);
        
        DropItemDisplayer.Singleton.UpdateTileDisplay(this);
        return success;
    }

    public bool PickupItem(int index)
    {
        bool success;
        if(DroppedItems == null)
            success = false;
        else if(0 <= index && index < DroppedItems.Count)
        {
            DroppedItems.RemoveAt(index);
            success = true;
        }
        else
            success = false;
        
        DropItemDisplayer.Singleton.UpdateTileDisplay(this);
        return success;
    }
}