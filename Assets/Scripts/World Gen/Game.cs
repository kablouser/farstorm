﻿using UnityEngine;
using TMPro;

public class Game : MonoBehaviour
{
    [SerializeField] private GameObject PlayerPrefab;

    [SerializeField] private Transform PlayerMenuPosition;
    [SerializeField] private Transform CameraMenuPosition;

    [SerializeField] private GameObject Menu;
    [SerializeField] private GameObject Hud;
    [SerializeField] private GameObject RestartMenu;
    [SerializeField] private GameObject NextLevelButton;

    [SerializeField] private bool TestMode;

    [SerializeField] private AudioSource MusicSource;
    [SerializeField] private AudioClip WinSound;
    [SerializeField] private AudioClip LoseSound;

    private Environment environment;
    private Mover playerMover;
    private DeathController playerDeathController;
    private CameraControls cameraControls;
    private LevelManager level;

    private bool menuShown;
    private bool playedMusic;

    private void Awake()
    {
        environment = GetComponentInChildren<Environment>();

        GameObject player = Instantiate(PlayerPrefab, transform);
        playerMover = player.GetComponent<Mover>();
        playerDeathController = player.GetComponent<DeathController>();

        level = GetComponent<LevelManager>();
    }

    private void Start()
    {
        cameraControls = CameraControls.Singleton;
        ShowMenu(true);
    }

    private void FixedUpdate()
    {
        if(menuShown == false)
        {
            bool playerDead = PlayerController.Singleton.isActiveAndEnabled == false;

            if(playerDead)
            {
                RestartMenu.SetActive(true);
                NextLevelButton.SetActive(false);

                if (playedMusic == false)
                {
                    MusicSource.PlayOneShot(LoseSound);
                    playedMusic = true;
                }
            }
            else if(environment.GetRemainingEnemies == 0)
            {
                RestartMenu.SetActive(false);
                NextLevelButton.SetActive(true);

                if (playedMusic == false)
                {
                    MusicSource.PlayOneShot(WinSound);
                    playedMusic = true;
                }
            }
        }
    }

    public void ShowMenu(bool show)
    {
        menuShown = show;
        playedMusic = false;

        if (Menu != null && Hud != null)
        {
            Menu.gameObject.SetActive(show);
            Hud.gameObject.SetActive(!show);
            RestartMenu.SetActive(false);
            NextLevelButton.SetActive(false);

            cameraControls.enabled = !show;

            if(show)
            {
                playerDeathController.SetAlive(false, false);
                playerMover.transform.position = PlayerMenuPosition.position;
                playerMover.transform.rotation = PlayerMenuPosition.rotation;
                environment.CleanUpWorld();

                cameraControls.transform.position = CameraMenuPosition.position;
                cameraControls.transform.rotation = CameraMenuPosition.rotation;                
            }
            else if(environment.Start.TryOccupy(playerMover))
            {
                playerDeathController.SetAlive(true, true);
                playerMover.transform.position = environment.Start.Position;
                playerMover.transform.rotation = Quaternion.identity;
                cameraControls.Focus(playerMover.transform.position);
            }            
        }
    }

    [ContextMenu("Next Level", false, -1)]
    public void AdvanceLevel()
    {
        playerDeathController.SetAlive(false, false);
        level.NextLevel();
        ShowMenu(false);
    }

    public void Restart()
    {
        playerMover.GetComponent<Status>().Reset();
        playerDeathController.SetAlive(false, false);
        if (TestMode == false)
        {
            playerMover.GetComponent<Inventory>().ClearInventory();
            PlayerController.Singleton.GemsPurse = 0;
        }
        level.ResetGame();
        ShowMenu(false);
    }

    public void Exit()
    {
        Application.Quit();
    }
}