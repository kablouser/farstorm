﻿using System.Collections.Generic;
using UnityEngine;

public class Environment : MonoBehaviour
{
    [SerializeField] private List<EnvironmentTile> TilePrefabs;
    [SerializeField] private List<GameObject> ObstaclePrefabs;
    [SerializeField] private float AccessiblePercentage = 0.7f;

    [SerializeField] private Transform EntityContainer;
    [SerializeField] private Transform ParticleContainer;

    private EnvironmentTile[][] mMap;
    private List<GameObject> entities;
    private List<GameObject> particles;

    private List<EnvironmentTile> mAll;
    private List<EnvironmentTile> mToBeTested;
    private List<EnvironmentTile> mLastSolution;

    private static Environment singletonReference;

    private const int infiniteLoopThreshold = 1000;

    // LEVEL CONFIGURATION
    public Vector2Int Size;
    public List<SpawnData> EntitySpawns;
    public Vector2Int PlayerStart;

    public readonly Vector3 NodeSize = Vector3.one * 0.9f; 
    public const float TileSize = 1.0f;
    public const float TileHeight = 0.25f;

    public EnvironmentTile Start { get; private set; }

    // Singleton referencer thing
    public static Environment Singleton
    { 
        get 
        {
            return singletonReference ? singletonReference : 
            singletonReference = FindObjectOfType<Environment>();
        }
    }

    public int GetRemainingEnemies
    {
        get
        {
            Fighter player = PlayerController.Singleton.GetComponent<Fighter>();
            int count = 0;
            for(int i = 0; i < entities.Count; i++)
            {
                if(entities[i] == null)
                {
                    entities.RemoveAt(i);
                    i--;
                }
                else
                {
                    if (player.IsFriendly(entities[i].GetComponent<Fighter>()) == false &&
                        entities[i].GetComponent<Status>().IsAlive == true)
                        count++;
                }
            }
            return count;
        }
    }

    [System.Serializable]
    public class SpawnData
    {
        public GameObject Prefab;
        public float Average;
        public int MinimumY;
        public int MaximumY;

        public SpawnData(GameObject prefab, float average, int minY, int maxY)
        {
            Prefab = prefab;
            Average = average;
            MinimumY = minY;
            MaximumY = maxY;
        }
    }

    private void Awake()
    {
        entities = new List<GameObject>();
        particles = new List<GameObject>();

        mAll = new List<EnvironmentTile>();
        mToBeTested = new List<EnvironmentTile>();
    }

    private void OnDrawGizmos()
    {
        // Draw the environment nodes and connections if we have them
        if (mMap != null)
        {
            for (int x = 0; x < Size.x; ++x)
            {
                for (int y = 0; y < Size.y; ++y)
                {
                    if (mMap[x][y].Connections != null)
                    {
                        for (int n = 0; n < mMap[x][y].Connections.Count; ++n)
                        {
                            Gizmos.color = Color.blue;
                            Gizmos.DrawLine(mMap[x][y].Position, mMap[x][y].Connections[n].Position);
                        }
                    }

                    // Use different colours to represent the state of the nodes
                    Color c = Color.white;
                    if ( !mMap[x][y].IsAccessible )
                    {
                        c = Color.red;
                    }
                    else
                    {
                        if(mLastSolution != null && mLastSolution.Contains( mMap[x][y] ))
                        {
                            c = Color.green;
                        }
                        else if (mMap[x][y].Visited)
                        {
                            c = Color.yellow;
                        }
                    }

                    Gizmos.color = c;
                    Gizmos.DrawWireCube(mMap[x][y].Position, NodeSize);
                }
            }
        }
    }

    private void Generate()
    {
        // Setup the map of the environment tiles according to the specified width and height
        // Generate tiles from the list of accessible and inaccessible prefabs using a random
        // and the specified accessible percentage
        mMap = new EnvironmentTile[Size.x][];

        int halfWidth = Size.x / 2;
        int halfHeight = Size.y / 2;
        Vector3 position = new Vector3( -(halfWidth * TileSize), 0.0f, -(halfHeight * TileSize) );

        for ( int x = 0; x < Size.x; ++x)
        {
            mMap[x] = new EnvironmentTile[Size.y];
            for ( int y = 0; y < Size.y; ++y)
            {
                bool isAccessible;
                if (x == 0 || y == 0 || x == Size.x - 1 || y == Size.y - 1)
                    // reduces chances of being blocked off
                    isAccessible = true;
                else
                    isAccessible = Random.value < AccessiblePercentage;

                
                EnvironmentTile prefab = TilePrefabs[Random.Range(0, TilePrefabs.Count)];
                EnvironmentTile tile = Instantiate(prefab, position, Quaternion.identity, transform);
                tile.Position = new Vector3( position.x + (TileSize / 2), TileHeight, position.z + (TileSize / 2));
                tile.IsAccessible = isAccessible;
                tile.gameObject.name = string.Format("Tile({0},{1})", x, y);
                mMap[x][y] = tile;
                mAll.Add(tile);
                position.z += TileSize;

                if(PlayerStart.x == x && PlayerStart.y == y)
                {
                    Start = tile;
                    isAccessible = tile.IsAccessible = true;
                }

                if(isAccessible == false)
                {
                    // place obstacle on top of it
                    GameObject obstaclePrefab = ObstaclePrefabs[
                        Random.Range(0, ObstaclePrefabs.Count)];

                    Instantiate(
                        obstaclePrefab, 
                        tile.Position, 
                        Quaternion.Euler(0,Random.Range(0,360),0), 
                        tile.transform.GetChild(0));
                }
            }

            position.x += TileSize;
            position.z = -(halfHeight * TileSize);
        }

        PopulateMap();
    }

    private void PopulateMap()
    {
        for(int i = 0; i < EntitySpawns.Count; i++)
        {
            int expectedSpawns = Mathf.RoundToInt(EntitySpawns[i].Average);
            int counter = 0;
            while(0 < expectedSpawns && counter < infiniteLoopThreshold)
            {
                int x = Random.Range(1, Size.x - 1);
                if (EntitySpawns[i].MinimumY <= 0)
                    EntitySpawns[i].MinimumY = 1;
                if (Size.y - 1 <= EntitySpawns[i].MaximumY)
                    EntitySpawns[i].MaximumY = Size.y - 2;

                int y = Random.Range(EntitySpawns[i].MinimumY, EntitySpawns[i].MaximumY + 1);

                if((x == PlayerStart.x && y == PlayerStart.y) ||
                   (mMap[x][y].IsAccessible == false) ||
                   (mMap[x][y].Occupier != null))
                   counter++;
                else
                {
                    GameObject newEntity = Instantiate(EntitySpawns[i].Prefab,
                        mMap[x][y].Position, Quaternion.identity, EntityContainer);
                    mMap[x][y].TryOccupy(newEntity.GetComponent<Mover>());
                    expectedSpawns--;
                    counter = 0;

                    entities.Add(newEntity);
                }
            }
        }
    }

    private void SetupConnections()
    {
        // Currently we are only setting up connections between adjacnt nodes
        for (int x = 0; x < Size.x; ++x)
        {
            for (int y = 0; y < Size.y; ++y)
            {
                EnvironmentTile tile = mMap[x][y];
                tile.Connections = new List<EnvironmentTile>();
                if (x > 0)
                {
                    tile.Connections.Add(mMap[x - 1][y]);
                }

                if (x < Size.x - 1)
                {
                    tile.Connections.Add(mMap[x + 1][y]);
                }

                if (y > 0)
                {
                    tile.Connections.Add(mMap[x][y - 1]);
                }

                if (y < Size.y - 1)
                {
                    tile.Connections.Add(mMap[x][y + 1]);
                }
            }
        }
    }

    private float Distance(EnvironmentTile a, EnvironmentTile b)
    {
        // Use the length of the connection between these two nodes to find the distance, this 
        // is used to calculate the local goal during the search for a path to a location
        float result = float.MaxValue;
        EnvironmentTile directConnection = a.Connections.Find(c => c == b);
        if (directConnection != null)
        {
            result = TileSize;
        }
        return result;
    }

    private float Heuristic(EnvironmentTile a, EnvironmentTile b)
    {
        // Use the locations of the node to estimate how close they are by line of sight
        // experiment here with better ways of estimating the distance. This is used  to
        // calculate the global goal and work out the best order to prossess nodes in
        return Vector3.Distance(a.Position, b.Position);
    }

    public EnvironmentTile TileAt(Vector3 position)
    {
        position /= TileSize;
        int x = Mathf.FloorToInt(position.x) + Size.x/2;
        int y = Mathf.FloorToInt(position.z) + Size.y/2;

        if(0 <= x && x < Size.x && 
           0 <= y && y < Size.y)
            return mMap[x][y];
        else
            return null;
    }

    public void ConfigureWorld(
        Vector2Int worldSize,
        List<SpawnData> entitySpawnChances, 
        Vector2Int playerStart)
    {
        Size = worldSize;
        EntitySpawns = entitySpawnChances;
        PlayerStart = playerStart;
    }

    public void GenerateWorld()
    {
        Generate();
        SetupConnections();
    }

    public void CleanUpWorld()
    {
        if (mMap != null)
        {
            for (int x = 0; x < Size.x; ++x)
                for (int y = 0; y < Size.y; ++y)
                    Destroy(mMap[x][y].gameObject);
            mMap = null;
        }
        
        foreach(GameObject entity in entities)
            if(entity) Destroy(entity);
        entities.Clear();

        foreach (GameObject particle in particles)
            if (particle) Destroy(particle);
        particles.Clear();
    }

    public List<EnvironmentTile> Solve(EnvironmentTile begin, EnvironmentTile destination, out bool success)
    {
        List<EnvironmentTile> result = null;
        if (begin != null && destination != null && destination.IsAccessible)
        {
            // Set all the state to its starting values
            mToBeTested.Clear();

            for( int count = 0; count < mAll.Count; ++count )
            {
                mAll[count].Parent = null;
                mAll[count].Global = float.MaxValue;
                mAll[count].Local = float.MaxValue;
                mAll[count].Visited = false;
            }

            // Setup the start node to be zero away from start and estimate distance to target
            EnvironmentTile currentNode = begin;
            currentNode.Local = 0.0f;
            currentNode.Global = Heuristic(begin, destination);

            // Maintain a list of nodes to be tested and begin with the start node, keep going
            // as long as we still have nodes to test and we haven't reached the destination
            mToBeTested.Add(currentNode);

            while (mToBeTested.Count > 0 && currentNode != destination)
            {
                // Begin by sorting the list each time by the heuristic
                mToBeTested.Sort((a, b) => (int)(a.Global - b.Global));

                // Remove any tiles that have already been visited
                mToBeTested.RemoveAll(n => n.Visited);

                // Check that we still have locations to visit
                if (mToBeTested.Count > 0)
                {
                    // Mark this note visited and then process it
                    currentNode = mToBeTested[0];
                    currentNode.Visited = true;

                    // Check each neighbour, if it is accessible and hasn't already been 
                    // processed then add it to the list to be tested 
                    for (int count = 0; count < currentNode.Connections.Count; ++count)
                    {
                        EnvironmentTile neighbour = currentNode.Connections[count];

                        if (!neighbour.Visited && neighbour.IsAccessible &&
                            neighbour.Occupier == null || neighbour == destination)
                            mToBeTested.Add(neighbour);
                        

                        // Calculate the local goal of this location from our current location and 
                        // test if it is lower than the local goal it currently holds, if so then
                        // we can update it to be owned by the current node instead 
                        float possibleLocalGoal = currentNode.Local + Distance(currentNode, neighbour);
                        if (possibleLocalGoal < neighbour.Local)
                        {
                            neighbour.Parent = currentNode;
                            neighbour.Local = possibleLocalGoal;
                            neighbour.Global = neighbour.Local + Heuristic(neighbour, destination);
                        }
                    }
                }
            }

            // Build path if we found one, by checking if the destination was visited, if so then 
            // we have a solution, trace it back through the parents and return the reverse route
            if (destination.Visited)
            {
                result = new List<EnvironmentTile>();
                EnvironmentTile routeNode = destination;
                if(destination.Occupier != null)
                    routeNode = destination.Parent;

                while (routeNode.Parent != null)
                {
                    result.Add(routeNode);
                    routeNode = routeNode.Parent;
                }
                result.Add(routeNode);
                result.Reverse();

                success = true;
            }
            else
            {
                success = false;
                Debug.LogWarning("Path Not Found");
            }
        }
        else
        {
            success = false;
            Debug.LogWarning("Cannot find path for invalid nodes");
        }

        mLastSolution = result;

        return result;
    }

    public List<EnvironmentTile> Solve(EnvironmentTile begin, EnvironmentTile destination)
    {
        return Solve(begin, destination, out bool x);
    }

    public bool GetMapExtents(out Vector3 bottomLeft, out Vector3 topRight)
    {
        if(mMap != null)
        {
            bottomLeft = mMap[0][0].Position;
            topRight = mMap[Size.x - 1][Size.y - 1].Position;
            return true;
        }
        else
        {
            bottomLeft = topRight = Vector3.zero;
            return false;
        }
    }

    public int RegisterParticle(GameObject particle)
    {
        particle.transform.parent = ParticleContainer;
        particles.Add(particle);
        return particles.Count - 1;
    }

    public void UnregisterParticle(int index)
    {
        if(0 <= index && index < particles.Count)
            particles.RemoveAt(index);
    }
}